--Usuario e perfil
SELECT email,nome,ocupacao,educacao,instituicao,uf,cidade
FROM usuario JOIN perfil
ON perfil.id = usuario_id;

--Documento de texto e paragrafo
SELECT nome,inicio,termino,texto,fonte,tamanho_fonte,caracter_especial
FROM documento_de_texto JOIN paragrafo;

--Folha de rosto
SELECT * FROM folha_de_rosto;

--Pagina de desenvolvimento
SELECT * FROM pagina_de_desenvolvimento;

--Capa
SELECT * FROM capa;

--Sumario
SELECT * FROM sumario;

--Resumo
SELECT * FROM resumo;

--Pagina Conclusao
SELECT * FROM pagina_conclusao;

--Lista
SELECT * FROM lista;

--Apencide
SELECT * FROM apendice;

--Referencia Bibliografica
SELECT * FROM referencia_bibliografica;

--Dedicatoria e Agradecimento
SELECT * FROM dedicatoria_agradecimento;