--Estátisticos
select count(*) from usuario;
select count(*) from perfil;
select count(*) from documento_de_texto;
select count(*) from paragrafo;
select count(*) from folha_de_rosto;
select count(*) from pagina_desenvolvimento;
select count(*) from capa;
select count(*) from sumario;
select count(*) from resumo;
select count(*) from pagina_conclusao;
select count(*) from lista;
select count(*) from pagina_introducao;
select count(*) from apendice;
select count(*) from referencia_bibliografica;
select count(*) from dedicatoria_agradecimento;

--Administrativo
select nome,ocupacao from perfil group by ocupacao;
select nome,ocupacao,instituicao from perfil group by instituicao;
select titulo,nome_autor,ano from folha_de_rosto group by ano;
select titulo,nome_orientador from folha_de_rosto group by nome_orientador;
select tema,objetivo from pagina_introducao group by tema;
select tema,objetivo,justificativa from pagina_introducao group by justificativa;
select objetivo,justificativa,procdimentos_adotados from pagina_introducao group by procdimentos_adotados;
select titulo,topico from pagina_conclusao group by topico;
select titulo,topico from pagina_conclusao group by titulo;
select texto,fonte,tamanho_fonte from paragrafo group by fonte;
select texto,caracter_especial from paragrafo group by caracter_especial;

--Analítico
select nome from documento_de_texto where inicio between '2015/04/20' and '2016/04/20';
select nome from documento_de_texto where termino between '2015/08/12' and '2016/08/12';
select titulo,nome_autor from folha_de_rosto where ano between '2015/01/31' and '2016/12/31';
select nome_orientador,objetivo,ano from folha_de_rosto group by ano;

--Sintético
select texto,fonte from paragrafo where fonte ='arial';
select nome,uf from perfil where uf ='mg';
select nome,usuario.id from perfil join usuario on perfil.id = usuario_id;
select * from perfil limit 5;

