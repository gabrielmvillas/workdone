<?php
session_start();
if(isset($_SESSION['userSession'])!="")
{
 header("Location: home.php");
}
include_once 'dbconnect.php';

if(isset($_POST['btn-signup']))
{
 $nome = $MySQLi_CON->real_escape_string(trim($_POST['nome']));
 $ocupacao = $MySQLi_CON->real_escape_string(trim($_POST['ocupacao']));
 $educacao = $MySQLi_CON->real_escape_string(trim($_POST['educacao']));
 $instituicao = $MySQLi_CON->real_escape_string(trim($_POST['instituicao']));
 $uf = $MySQLi_CON->real_escape_string(trim($_POST['uf']));
 $cidade = $MySQLi_CON->real_escape_string(trim($_POST['cidade']));
 $email = $_GET['email'];

 $id_query = $MySQLi_CON->query("SELECT * FROM usuario WHERE email='$email'");
 $row=$id_query->fetch_array();
 $id_usuario = $row['id'];

 $query = "INSERT INTO perfil(id,nome,ocupacao,educacao,instituicao,uf,cidade,usuario_id) VALUES(null,'$nome','$ocupacao','$educacao','$instituicao','$uf','$cidade','$id_usuario')";


 if($MySQLi_CON->query($query))
 {
   $msg = "<div class='row'>
   <div class='alert alert-success col-md-4 col-md-offset-4'>
     <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Perfil registrado com sucesso!
   </div>
 </div>";
  header("Location: index.html?email=$email");
}
else
{
 $msg = "<div class='row'>
 <div class='alert alert-danger col-md-4 col-md-offset-4'>
   <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Erro no registro do perfil!
 </div>
</div>";
}

$MySQLi_CON->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>WorkDone</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <!--   <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">  -->
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">

</head>
<body id="page-top">
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand page-scroll" href="#page-top">WorkDone</a>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>

  <section id="sim" class="bg-dark">
    <aside>
      <div class="container text-center">
        <div class="call-to-action">

          <h2><br>Informações do perfil</h2>

          <?php
          if(isset($msg)){
           echo $msg;
         }
         else{
           ?>
           <div class="row">
             <div class='alert alert-info col-md-4 col-md-offset-4'>
              <span class='glyphicon glyphicon-asterisk'></span> &nbsp; Todos os campos são obrigatorios
            </div>
          </div>
          <?php
        }
        ?>
        <form class="form-horizontal" action="" method="post">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Nome Completo</label>
            <div class="col-sm-4 " >
              <input type="text" class="form-control" placeholder="Nome Completo" name="nome" required  />
              <span id="check-e"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Ocupação</label>
            <div class="col-sm-4 " >
              <input type="text" class="form-control" placeholder="Ocupação" name="ocupacao" required  />
              <span id="check-e"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Educação</label>
            <div class="col-sm-4 " >
              <input type="text" class="form-control" placeholder="Educação" name="educacao" required  />
              <span id="check-e"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Instituição</label>
            <div class="col-sm-4 " >
              <input type="text" class="form-control" placeholder="Instituição" name="instituicao" required  />
              <span id="check-e"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Estado</label>
            <div class="col-sm-4 " >
              <select class="form-control" name="uf">
                <option value="">Selecione Seu Estado</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espirito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MT">Mato Grosso</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
              </select>
              <span id="check-e"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Cidade</label>
            <div class="col-sm-4 " >
              <input type="text" class="form-control" placeholder="Cidade" name="cidade" required  />
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="btn-signup">
              <span class="glyphicon glyphicon-log-in"></span> &nbsp; Cadastrar
            </button><br><br>
            <a href="signin.php" style="float:center; color:#fff;">Ja tenho uma conta</a> 
          </div> 
        </form>
      </div>
    </div>
  </aside>
</section>
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <h2 class="section-heading">Contate-nos</h2>
        <hr class="primary">
        <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
      </div>
      <div class="col-lg-4 col-lg-offset-2 text-center">
        <i class="fa fa-phone fa-3x sr-contact"></i>
        <p>(31) 9 9346-5930</p>
      </div>
      <div class="col-lg-4 text-center">
        <i class="fa fa-envelope-o fa-3x sr-contact"></i>
        <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
      </div>
    </div>
  </div>
</section>



<!-- 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="js/scrollreveal.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.fittext.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/creative.js"></script>
</body>
</html>