<?php
session_start();
include("fpdf/fpdf.php");
include_once 'dbconnect.php';



$documento_id=$_GET['id'];

$querydoc = $MySQLi_CON->query("SELECT * FROM documento WHERE usuario_id=".$_SESSION['userSession']);
$docRow=$querydoc->fetch_array();
// print_r($docRow);
// $docRow['nome'];

$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);/*SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=*/
$userRow=$query->fetch_array();

$check_intro = $MySQLi_CON->query("SELECT id FROM introducao WHERE documento_id='$documento_id'");
$introducaorow = mysqli_fetch_row($check_intro);
$idintro=$introducaorow['0'];

$check_capa = $MySQLi_CON->query("SELECT id FROM capa WHERE documento_id='$documento_id'");
$caparow3 = mysqli_fetch_row($check_capa);
$idcapa=$caparow3['0'];

$check_paragrafo_intro = $MySQLi_CON->query("SELECT introducao_id FROM paragrafo_introducao");
$pag_introrow = mysqli_fetch_row($check_paragrafo_intro);
$id_p_intro=$pag_introrow['0'];

$check_paragrafo_desenvolvimento = $MySQLi_CON->query("SELECT desenvolvimento_id FROM paragrafo_desenvolvimento");
$pag_devrow = mysqli_fetch_row($check_paragrafo_desenvolvimento);
$id_p_dev=$pag_devrow['0'];

$check_paragrafo_conclusao = $MySQLi_CON->query("SELECT conclusao_id FROM paragrafo_conclusao");
$pag_concrow = mysqli_fetch_row($check_paragrafo_conclusao);
$id_p_conc=$pag_concrow['0'];

$check_topico = $MySQLi_CON->query("SELECT sumario_id FROM topico");
$toprow = mysqli_fetch_row($check_topico);
$id_topico=$toprow['0'];

$check_subtopico = $MySQLi_CON->query("SELECT topico_id FROM subtopico");
$subtoprow = mysqli_fetch_row($check_subtopico);
$id_subtopico=$subtoprow['0'];


$selectCapa = $MySQLi_CON->query("SELECT titulo, data, localizacao, instituicao FROM capa WHERE documento_id='$documento_id'");
$caparow2 = mysqli_fetch_row($selectCapa);

$selectIntegrantes = $MySQLi_CON->query("SELECT nome, numero, turma FROM integrantes_capa WHERE capa_id='$idcapa'");

$countIntegrantes = $selectIntegrantes->num_rows;


$selectIntroducao = $MySQLi_CON->query("SELECT nome FROM introducao WHERE documento_id='$documento_id'");
$introRow = mysqli_fetch_row($selectIntroducao);

$selectPagIntroducao = $MySQLi_CON->query("SELECT texto FROM paragrafo_introducao WHERE introducao_id='$id_p_intro'");
$countIntro=$selectPagIntroducao->num_rows;

$selectPagDesenvolvimento = $MySQLi_CON->query("SELECT texto FROM paragrafo_desenvolvimento WHERE desenvolvimento_id='$id_p_dev'");
$countDev=$selectPagDesenvolvimento->num_rows;

$selectPagConclusao = $MySQLi_CON->query("SELECT texto FROM paragrafo_conclusao WHERE conclusao_id='$id_p_conc'");
$countConc=$selectPagConclusao->num_rows;

$selectTopico = $MySQLi_CON->query("SELECT nome FROM topico WHERE sumario_id='$id_topico'");
$countTopico=$selectTopico->num_rows;

$selectsubTopico = $MySQLi_CON->query("SELECT nome FROM subtopico WHERE topico_id='$id_subtopico'");
$countsubTopico=$selectsubTopico->num_rows;

$tudo;

class PDF extends FPDF
{
	function Header()
	{
		global $title;

    // Arial bold 15
		$this->SetFont('Arial','B',22);
    // Calculate width of title and position
		$w = $this->GetStringWidth($title)+6;
		$this->SetX((210-$w)/2);
    // Colors of frame, background and text
		$this->SetDrawColor(255,255,255);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
    // Thickness of frame (1 mm)
		$this->SetLineWidth(1);
    // Title
		$this->Cell($w,9,$title,1,1,'C',true);
    // Line break
		$this->Ln(10);
	}

	function Footer()
	{
    // Position at 1.5 cm from bottom
		$this->SetY(-15);
    // Arial italic 8
		$this->SetFont('Arial','I',8);
    // Text color in gray
		$this->SetTextColor(128);
    // Page number
		$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}

	function ChapterTitle($label)
	{
		// Calculate width of title and position
		$w = $this->GetStringWidth($label)+0;
		$this->SetX(90);
    		// Arial 12
		$this->SetFont('Arial','',12);
    		// Background color
		$this->SetFillColor(255,255,255);
    		// Title
		$this->Cell(0,0,"$label",0,1,'L',true);
    		// Line break
		$this->Ln(4);
	}

	function ChapterBody()
	{
    // Read text file
		$txt = "Pellentesque ac blandit mauris, id commodo dolor. Sed eu tempor nulla. Duis ultricies justo vitae viverra eleifend. Donec libero felis, pharetra at elit sit amet, facilisis pulvinar odio. Sed vulputate elit at ex luctus, at sodales tortor posuere. Donec non ultricies sem. Duis a facilisis sem. Phasellus bibendum cursus justo a sodales. Maecenas porta congue varius. Pellentesque laoreet vehicula nibh, vel sollicitudin neque dignissim ut. Ut massa quam, egestas a tempor ac, porta vitae odio. Proin at convallis est.";
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
    // Mention in italics
		$this->SetFont('','I');
		$this->Cell(0,5,'');
	}


	function PrintChapter($title, $file)
	{
		$this->AddPage();
		$this->ChapterTitle($title);
		$this->ChapterBody($file);
	}



	function CapaTitle($label)
	{
    // Arial 12
		$this->SetFont('Arial','',12);
    // Background color
		$this->SetFillColor(200,220,255);
    // Title
		$this->Cell(0,6,"$label",0,1,'L',true);
    // Line break
		$this->Ln(4);
	}
	function CapaBody($data, $localizacao, $instituicao)
	{
    // Read text file
		$txt = $data."\n".$localizacao."\n".$instituicao."\n";
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
    // Mention in italics
		$this->SetFont('','I');
		$this->Cell(0,5,'');
	}
	function IntegranteBody($nome, $numero, $turma)
	{
		// Calculate width of title and position;
		$this->SetX(50);
    // Read text file
		$txt = "Nome: ".$nome." Numero: ".$numero." Turma: ".$turma."";
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
    // Mention in italics
		$this->SetFont('','I');
		$this->Cell(0,5,'');
	}
	function IntegrantesTitle()
	{
    // Arial 12
		$this->SetFont('Arial','',12);
    // Background color
		$this->SetFillColor(200,220,255);
    // Title
		$this->Cell(0,6,"Integrantes",0,1,'L',true);
    // Line break
		$this->Ln(4);
	}
	function PrintCapa($nome, $data, $localizacao, $instituicao)
	{
		$this->AddPage();
		$this->ChapterTitle($nome);
		$this->CapaBody($data, $localizacao, $instituicao);

	}
	function PrintCapaIntegrante($nome, $numero, $turma)
	{
		$this->IntegranteBody($nome, $numero, $turma);
	}


	function SumarioTitle($label)
	{
		// Calculate width of title and position
		$w = $this->GetStringWidth($label)+0;
		$this->SetX(90);
    		// Arial 12
		$this->SetFont('Arial','',18);
    		// Background color
		$this->SetFillColor(255,255,255);
    		// Title
		$this->Cell(0,0,"$label",0,1,'L',true);
    		// Line break
		$this->Ln(4);
	}

	function SumarioBody($texto)
	{
    // Read text file
		$txt = $texto;
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
	}
	function SubTopBody($texto)
	{
	// Calculate width of title and position
		$w = $this->GetStringWidth($texto)+0;
		$this->SetX(90);

    // Read text file
		$txt = $texto;

    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
	}


	function PrintSumarioTitulo($title)
	{
		$this->AddPage();
		$this->SumarioTitle($title);
	}
	function PrintSumarioTopico($paragrafo)
	{
		$this->SumarioBody($paragrafo);
	}
	function PrintSumarioSubTopico($paragrafo)
	{
		$this->SubTopBody($paragrafo);
	}




	function IntroTitle($label)
	{
		// Calculate width of title and position
		$w = $this->GetStringWidth($label)+0;
		$this->SetX(90);
    		// Arial 12
		$this->SetFont('Arial','',18);
    		// Background color
		$this->SetFillColor(255,255,255);
    		// Title
		$this->Cell(0,0,"$label",0,1,'L',true);
    		// Line break
		$this->Ln(4);
	}

	function IntroBody($texto)
	{
    // Read text file
		$txt = $texto;
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
	}


	function PrintIntroTitulo($title)
	{
		$this->AddPage();
		$this->IntroTitle($title);
	}
	function PrintIntroParagrafo($paragrafo)
	{
		$this->IntroBody($paragrafo);
	}


	function DesenvolvimentoTitle($label)
	{
		// Calculate width of title and position
		$w = $this->GetStringWidth($label)+0;
		$this->SetX(90);
    		// Arial 12
		$this->SetFont('Arial','',18);
    		// Background color
		$this->SetFillColor(255,255,255);
    		// Title
		$this->Cell(0,0,"$label",0,1,'L',true);
    		// Line break
		$this->Ln(4);
	}

	function DesenvolvimentoBody($texto)
	{
    // Read text file
		$txt = $texto;
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
	}


	function PrintDesenvolvimentoTitulo($title)
	{
		$this->AddPage();
		$this->IntroTitle($title);
	}
	function PrintDesenvolvimentoParagrafo($paragrafo)
	{
		$this->IntroBody($paragrafo);
	}


	function ConclusaoTitle($label)
	{
		// Calculate width of title and position
		$w = $this->GetStringWidth($label)+0;
		$this->SetX(90);
    		// Arial 12
		$this->SetFont('Arial','',18);
    		// Background color
		$this->SetFillColor(255,255,255);
    		// Title
		$this->Cell(0,0,"$label",0,1,'L',true);
    		// Line break
		$this->Ln(4);
	}

	function ConclusaoBody($texto)
	{
    // Read text file
		$txt = $texto;
    // Times 12
		$this->SetFont('Times','',12);
    // Output justified text
		$this->MultiCell(0,5,$txt);
    // Line break
		$this->Ln();
	}


	function PrintConclusaoTitulo($title)
	{
		$this->AddPage();
		$this->IntroTitle($title);
	}
	function PrintConclusaoParagrafo($paragrafo)
	{
		$this->IntroBody($paragrafo);
	}


}
$pdf = new PDF();
$title = $docRow['nome'];
$pdf->SetTitle($title);
$pdf->PrintCapa($caparow2['0'],$caparow2['1'],$caparow2['2'],$caparow2['3']);
for ($j=1; $j <= $countIntegrantes ; $j++) { 
	$integrantesRow = mysqli_fetch_assoc($selectIntegrantes);
	$pdf->PrintCapaIntegrante($integrantesRow['nome'], $integrantesRow['numero'], $integrantesRow['turma']);
}
$pdf->PrintSumarioTitulo('Sumario');

$selectsum = $MySQLi_CON->query("SELECT id FROM sumario WHERE documento_id='$documento_id'");
$idsum=mysqli_fetch_row($selectsum);

$selecttop = $MySQLi_CON->query("SELECT * FROM topico WHERE sumario_id='$idsum[0]'");
$linhas=$selecttop->num_rows;

for($x = 0; $x < $linhas; $x++){
	$linhatop = mysqli_fetch_assoc($selecttop);
	$pdf->PrintDesenvolvimentoParagrafo(($x+1).")".$linhatop['nome']);
	$selectsubtop = $MySQLi_CON->query("SELECT * FROM subtopico WHERE topico_id='$linhatop[id]'");
	$linhassub=$selectsubtop->num_rows;

	if ($linhassub !=0)
	{

		for($y = 0; $y < $linhassub; $y++){
			$linhasubtop = mysqli_fetch_assoc($selectsubtop);
			$pdf->PrintSumarioSubTopico(($y+1).":".$linhasubtop['nome']);
		}
	}
}

$pdf->PrintIntroTitulo('Introdução');

for ($x=1; $x <= $countIntro ; $x++) { 
	$introPagRow = mysqli_fetch_row($selectPagIntroducao);
	$pdf->PrintIntroParagrafo($introPagRow[0]);
}





$pdf->PrintDesenvolvimentoTitulo('Desenvolvimento');

for ($x=1; $x <= $countIntro ; $x++) { 
	$devPagRow = mysqli_fetch_row($selectPagDesenvolvimento);
	$pdf->PrintDesenvolvimentoParagrafo($devPagRow[0]);
}
$pdf->PrintConclusaoTitulo('Conclusão');

for ($x=1; $x <= $countIntro ; $x++) { 
	$concPagRow = mysqli_fetch_row($selectPagConclusao);
	$pdf->PrintConclusaoParagrafo($concPagRow[0]);
}

$pdf->Output();
?>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
</head>