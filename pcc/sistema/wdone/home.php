<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['userSession']))
{
 header("Location: signin.php");
}

$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);/*SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=*/
$userRow=$query->fetch_array();
$select = $MySQLi_CON->query("SELECT * FROM documento WHERE usuario_id=".$_SESSION['userSession']);
$linhas=$select->num_rows;

if(isset($_POST['btn-inserirdocumento']))
{
  $nome=$_POST['titulo'];
  $data = date("Y-m-d");
   $querydev = $MySQLi_CON->query("INSERT INTO documento(id, nome, data, usuario_id) VALUES(null,'$nome','$data', ".$_SESSION['userSession'].")");


   $querydoc = $MySQLi_CON->query("SELECT id FROM documento WHERE data='$data' AND nome='$nome'");
   $docrow = mysqli_fetch_row($querydoc);
   $documentoid=$docrow['0'];

   $querydev = $MySQLi_CON->query("INSERT INTO sumario(id, documento_id) VALUES(null,$documentoid");

   header("Location: home.php");
   echo $documentoid;
} 

$MySQLi_CON->close();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Workdone</title>

  <!-- Bootstrap Core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="bootstrap/css/sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>

      <body>
        <div id="wrapper">
          <!-- Sidebar -->
          <div id="sidebar-wrapper">
            <ul class="sidebar-nav bd-dark">
              <li class="sidebar-brand">
                <a href="home.php">
                  Workdone
                </a>
              </li>
            </li>
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
              <img src="<?php
              if($userRow['avatar']== NULL)
              {
                echo "img/avatar/default.jpg";
              } 
              else
              {
                echo "img/avatar/".$userRow['avatar'];
              }
              ?>" 
              class="img-responsive img-circle" style="height:150px; width:150px;"alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
              <div class="profile-usertitle-name">
               <?php echo $userRow['nome']; ?>
             </div>
             <div class="profile-usertitle-job">
              <?php echo $userRow['ocupacao']; ?>
            </div>
          </div>
          <!-- END SIDEBAR USER TITLE -->
          <!-- SIDEBAR MENU -->
          <div class="profile-usermenu">
            <ul class="nav">
              <li class="active">
              </li>
              <li>
                <a href="home.php">
                  <i class="glyphicon glyphicon-file"></i>
                  Meus Projetos </a>
                </li>
                <li>
                  <a href="editarperfil.php">
                    <i class="glyphicon glyphicon-user"></i>
                    Perfil </a>
                  </li>
                  <li>
                    <a href="contato.php">
                      <i class="glyphicon glyphicon-flag"></i>
                      Contato </a>
                    </li>
                    <?php
                    if ($userRow['tipo_do_usuario_id'] == 1)
                    { 
                      echo "<li>";
                      echo "<a href=\"admin.php\">";
                      echo "<i class=\"glyphicon glyphicon-list-alt\"></i>";
                      echo "Gestão de Usuários </a>";
                      echo "</li>";
                    }
                    ?>
                    <li>
                      <a href="logout.php?logout">
                        <i class="glyphicon glyphicon-log-out"></i>
                        Sair </a>
                      </li>

                    </ul>
                  </div>
                  <!-- END MENU -->
                </div>

                <!-- /#sidebar-wrapper -->


                <!-- Page Contenst -->

                <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></a>
                <section id="gerenciador" class="bg-light">
                  <div class="container">
                    <div class="row-centered">
                      <br>
                      <br>
                      <div class="panel panel-primary">
                        <div class="panel-heading">Editar</div>
                        <div class="panel-body">
                          <div class="row">
                            <?php
                            if ($linhas > 0)
                            {
                              for($x = 0; $x < $linhas; $x++){
                                $linha = mysqli_fetch_assoc($select);
                                
                                echo "<div style=\"margin-left:2%;\"class=\"col-md-3 well well-lg\">";
                                echo "<img src=\"img/manual_a4.jpg\" alt=\"\" class=\"img-rounded img-responsive\">";
                                echo "<label>".$linha['nome']."</label><br>";
                                echo "<a href=\"editor.php?id=".$linha['id']."\"><button type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Deletar Documento\" class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-pencil\"></i>Alterar</button></a>";
                                echo "<a href=\"deletar_documento.php?id=".$linha['id']."\"><button type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Criar um Novo Documento\" class=\"btn btn-danger\"><i class=\"glyphicon glyphicon-trash\"></i>Deletar</button></a>";
                                echo "</div>";
                                
                              }
                            }
                            else
                            {
                              echo "Você não póssui nenhum arquivo de texto.<br><br>";
                              echo " <a href=\"editor.php\"><button type=\"button\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Criar um Novo Documento\" class=\"btn btn-primary btn-circle btn-xl \"><i class=\"glyphicon glyphicon-plus\"></i></button></a>";

                            }
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section id="contact">
                  <div class="container">
                    <div class="row">
                      <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="section-heading">Contate-nos</h2>
                        <hr class="primary">
                        <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
                      </div>
                      <div class="col-lg-4 col-lg-offset-2 text-center">
                        <i class="fa fa-phone fa-3x sr-contact"></i>
                        <p>(31) 9 9346-5930</p>
                      </div>
                      <div class="col-lg-4 text-center">
                        <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                        <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
                      </div>
                    </div>
                  </div>
                </section>
                
                <a class="dropdown-item" href="#"><button type="button" href="capa-modal.php" title="Conclusão"class="btn btn-primary btn-xl btn-circle fixedbutton"data-toggle="modal" data-target="#inseredoc"><i class="glyphicon glyphicon-plus"></i></button></a><br>
                <!-- Modal Sumario -->
                <div class="modal fade" id="inseredoc" role="dialog">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Inserir Documento</h4>
                      </div>
                      <div class="modal-body text-center">
                        <form class="form-horizontal" action="home.php" method="post">
                          <div class="form-group">
                            <h3 class="center-block">Nome do Documento</h3>
                            <div class="col-sm-12" >
                              <input type="text" class="form-control" placeholder="Título" name="titulo"  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                          <div class="form-group">
                            <button type="submit" class="btn btn-success pull-right" name="btn-inserirdocumento">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Inserir
                            </button>
                          </div>
                          <br>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /Modal Sumario -->
            <!-- /#page-content-wrapper -->

          </div>
          <!-- /#wrapper -->

          <!-- jQuery -->
          <script src="bootstrap/js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="bootstrap/js/bootstrap.min.js"></script>

          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>

        </body>

        </html>