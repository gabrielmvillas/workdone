

INSERT INTO tipo_do_usuario(id,descricao)VALUES(NULL,"administrador" );
INSERT INTO tipo_do_usuario(id,descricao)VALUES(NULL,"usuario_comum" );
INSERT INTO tipo_do_usuario(id,descricao)VALUES(NULL,"usuario_premium" );

INSERT INTO usuario(id,email,senha,tipo_do_usuario_id) VALUES(null,"cassiana@gmail.com","123456",1);

INSERT INTO perfil(id,nome,ocupacao,educacao,instituicao,uf,cidade,usuario_id) VALUES(null,
"Cassiana Duarte da Cruz","estudante","Pedagogia","Universidade Federal de S�o Carlos","SP","Sao Carlos",1);

INSERT INTO documento_de_texto(id,nome,inicio,termino,perfil_id) VALUES(null,"
tcc Pedagogia",'2007-02-24','2007-12-03',1);

INSERT INTO documento_de_texto(id,nome,inicio,termino,perfil_id) VALUES(null,"
tcc cysbycb",'2007-03-24','2007-03-03',1);



INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES (null,
'Reda��o final do Trabalho de Conclus�o de Curso �A contribui��o dos di�rios reflexivos no processo 
de forma��o de uma professora iniciante de L�ngua Estrangeira (Ingl�s) em uma escola de Educa��o 
Infantil Bil�ng�e� apresentado como requisito para obten��o do grau de Licenciatura em Pedagogia da
Universidade Federal de S�o Carlos.','arial',20,null,1);

INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'Em meu percurso metodol�gico optei por fazer uma pesquisa de campo que, al�m de ter me possibilitado coletar 
dados importantes (registrados nos di�rios reflexivos), tamb�m me permitiu presenciar a pr�tica de ensino de 
l�ngua inglesa de professoras em exerc�cio e o seu papel no processo de aquisi��o/aprendizagem dessa l�ngua pelas 
crian�as.','arial',20,null,1);


INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES (null,
'A contribui��o dos di�rios reflexivos no processo de forma��o de uma professora iniciante de L�ngua Estrangeira 
(Ingl�s) em uma Escola de Educa��o InfantilBil�ng�e.','arial',20,null,1);


INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id) VALUES(null,
'Introdu��o','arial',20,null,1);

INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'Este cap�tulo tem por objetivo apresentar algumas das contribui��es que os di�rios reflexivos trouxeram para 
a minha forma��o enquanto futura professora no de l�ngua inglesa (LI) para crian�as. Para tanto, comparei 
o jeito de ensinar LI das professoras que acompanhei na pesquisa de campo com a forma como eu aprendi o
idioma e, por fim, analisei em que medida o processo de observa��o e reflex�o contribuiu na minha vis�o de 
ensinar/aprender L�ngua Estrangeira.','arial',20,null,1);


INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'O presente trabalho com o tema �A contribui��o dos di�rios reflexivos no processo de forma��o de uma 
professora iniciante de L�ngua Estrangeira (Ingl�s) em uma Escola de Educa��o Infantil Bil�ng�e� 
tem por objetivo atender as exig�ncias para o t�rmino do curso de Licenciatura em Pedagogia da Universidade
Federal de S�o Carlos (UFSCar).','arial',20,null,1);


INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'ALMEIDA FILHO, Jos� Carlos P. de. Dimens�es comunicativas no ensino de l�nguas. Campinas, SP: Pontes, 1993.
(Linguagem - Ensino).','arial',20,null,1);

INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'� Acuidade sensorial � a capacidade de aprendizagem da l�ngua dos adultos � prejudicada pela deterioriza��o 
de suas habilidades para perceber e segmentar os sons da L2; � Explica��o Neurol�gica � h� mudan�as na 
estrutura neurol�gica do c�rebro (perda de plasticidade, lateraliza��o e maturidade cerebral) em determinadas
idades que afetam as habilidades de os aprendizes adquirirem a pron�ncia e a gram�tica da L2;� Fatores 
afetivos � as crian�as s�o mais fortemente motivadas a interagir com falantes nativos e t�m menos ansiedade 
as se comunicar na L2;','arial',20,null,1);

INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'De acordo com autoras, conclui-se que quanto ao ensino de l�nguas o que parece fazer a diferen�a na 
rela��o idade e aquisi��o � a situa��o de aprendizagem e n�o a capacidade de aprender. Sem quantidade e 
diversidade de insumo, sem m�todos apropriados para trabalhar com crian�as e sem professores com dom�nio 
ling��stico e pedag�gico � em v�o submeter crian�as de tenra idade ao estudo de L2.','arial',20,null,1);

INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'teste_resumo','arial',20,null,1); 

INSERT INTO paragrafo(id,texto,fonte,tamanho_fonte,caracter_especial,documento_de_texto_id ) VALUES(null,
'teste_dedicatoria','arial',20, null,1);

INSERT INTO capa(id,titulo,autor,ano,curso,instituicao,cidade,paragrafo_id) VALUES(NULL,'A contribui��o dos 
di�rios reflexivos no processo de forma��o de uma professora iniciante de L�ngua Estrangeira (Ingl�s) em uma 
Escola de Educa��o Infantil Bil�ng�e','Cassiana Duarte da Cruz','2007','Pedagogia','Universidade Federal de S�o 
Carlos Centro de Educa��o e Ci�ncias Humanas','S�o Carlos',1);


INSERT INTO capa(id,titulo,autor,ano,curso,instituicao,cidade,paragrafo_id) VALUES(NULL,'A contribui��o dos 
di�rios reflexivos no processo de forma��o de uma professora iniciante de L�ngua Estrangeira (Ingl�s) em uma 
Escola de Educa��o Infantil Bil�ng�e','Ca,kjdasbvjha Cruz','2007','Pedagogia','Universidade Federal de S�o 
Carlos Centro de Educa��o e Ci�ncias Humanas','kjhgej',1);

INSERT INTO capa(id,titulo,autor,ano,curso,instituicao,cidade,paragrafo_id) VALUES(NULL,'A contribui��o dos 
di�rios reffabfjewvos no processo de forma��o de uma professora iniciante de L�ngua Estrangeira (Ingl�s) em uma 
Escola de Educa��o Infantil Bil�ng�e','Cassiana fkesbfsekf da Cruz','2007','Pedagogia','Universidade Federal de S�o 
Carlos Centro de Educa��o e Ci�ncias Humanas','S�o Carlos',1);


INSERT INTO folha_de_rosto (id,titulo,nome_orientador,nome_do_autor,objetivo,ano,cidade,nota_descritiva,pe_pagina,
paragrafo_id) VALUES(null,'Trabalho de Conclus�o do Curso de Licenciatura em Pedagogia','Prof.Dr. Ademar da Silva',
'Cassiana Duarte da Cruz','Licenciatura em Pedagogia','2007','S�O CARLOS','Reda��o final do Trabalho de Conclus�o
de Curso �A contribui��o dos di�rios reflexivos no processo de fomcbdhb\hgvchvefessora iniciante de L�ngua
Estrangeira (Ingl�s) em uma escola de Educa��o Infantil Bil�ng�e� apresentado como requisito para obten��o do grau 
de Licenciatura em Pedagogia da Universidade Federal de S�o Carlos.','TESTE',2);


INSERT INTO folha_de_rosto (id,titulo,nome_orientador,nome_do_autor,objetivo,ano,cidade,nota_descritiva,pe_pagina,
paragrafo_id) VALUES(null,'Trabalho de Conclus�o do Curso de Licenciatura em Pedagogia','Prof.Dr. Ademar da Silva',
'Cassiana Duarte da Cruz','Licenciatura em Pedagogia','2007','S�O CARLOS','Reda��o final do Trabalho de Conclus�o
de Curso �A contribui��o dos di�rios reflexivosvjhbrhjprocesso de fomcbdhb\hgvchvefessora iniciante de L�ngua
Estrangeira (Ingl�s) em uma escola de Educa��o Infantil Bil�ng�e� apresentado como requisito para obten��o do grau 
de Licenciatura em Pedagogia da Universidade Federal de S�o Carlos.','TESTE',2);

INSERT INTO folha_de_rosto (id,titulo,nome_orientador,nome_do_autor,objetivo,ano,cidade,nota_descritiva,pe_pagina,
paragrafo_id) VALUES(null,'Trabalho de Conclus�o do Curso de Licenciatura em Pedagogia','Prof.Dr. Ademar da Silva',
'Cassiana Duarte da Cruz','Licenciatura em Pedagogia','2007','S�O CARLOS','Reda��o final do Trabalho de Conclus�o
de Curso �A contribui��o dos di�rios reflexivos no processo de fomcbdhb\hgvchvefessora iniciante de L�ngua
Estrangeira (Ingl�s) em uma escola de Educa��o Infantil Bil�ng�e� apresentado como requisito para obten��o do grau 
de Licenciatura em Pedagogia da Universidade Federal de S�o Carlos.','TESTE',2);

INSERT INTO pagina_desenvolvimento(id,titulo,topico,semi_topico,numero_pagina,margem,pe_pagina,paragrafo_id) VALUES
(null,'Cap�tulo 1 � O Percurso Metodol�gico','O Percurso Metodol�gico','O Percurso Metodol�gico',8,10,'TESTE',3);

INSERT INTO sumario(id,nome_sessao,numero_sessao,numero_pagina,paragrafo_id) VALUES (null,'Introdu��o',1,5,4);

INSERT INTO pagina_conclusao(id,titulo,topico,semi_topico,numero_pagina,margem,pe_pagina,paragrafo_id) VALUES(null,
'Cap�tulo 3 - Apresenta��o e an�lise dos dados','Di�rios reflexivos e a forma��o de uma professora iniciante no 
ensino de l�nguas (ingl�s)','Di�rios reflexivos e a forma��o de uma professora iniciante no ensino de l�nguas
(ingl�s)',21,10,'TESTE',5);

INSERT INTO pagina_introducao (id,tema,objetivo,procedimentos_adotados,numero_pagina,delimitacao,justificativa,
margem,pe_pagina,paragrafo_id) VALUES(null,'A contribui��o dos di�rios reflexivos no processo de forma��o de uma 
professora iniciante de L�ngua Estrangeira (Ingl�s) em uma Escola de Educa��o Infantil Bil�ng�e','atender as 
exig�ncias para o t�rmino do curso de Licenciatura em Pedagogia da Universidade Federal de S�o Carlos (UFSCar).',
'pesquisas',7,'falta de dados','falta de infra estrutura',10,'teste',6);

INSERT INTO referencia_bibliografica (id,numero_pagina,margem,paragrafo_id) VAlUES(null,34,10,'teste',7);

INSERT INTO lista(id,titulo_elementos,margem,numero_paginas,paragrafo_id) VALUES(null,'A diferen�a da 
aprendizagem entre adultos e crian�as',10,13,8);

INSERT INTO apendice(id,margem,numero_pagina,paragrafo_id) VALUES(null,10,14,9);

INSERT INTO resumo (id,margem,numero_pagina,titulo,pe_pagina,paragrafo_id) VALUES(null,10,3,'teste','teste',10);

INSERT INTO dedicatoria_agradecimento(id,numero_pagina,margem,pe_pagina,paragrafo_id) VALUES(null,34,10,'teste',11);