<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['userSession']))
{
 header("Location: signin.php");
}
$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);
$userRow=$query->fetch_array();
if(isset($_POST['btn-foto']))
{
  $allowedExts = array("gif", "jpeg", "jpg", "png");
  $temp = explode(".", $_FILES["file"]["name"]);
  $extension = end($temp);
  if ((($_FILES["file"]["type"] == "image/gif")
    || ($_FILES["file"]["type"] == "image/jpeg")
    || ($_FILES["file"]["type"] == "image/jpg")
    || ($_FILES["file"]["type"] == "image/pjpeg")
    || ($_FILES["file"]["type"] == "image/x-png")
    || ($_FILES["file"]["type"] == "image/png"))
    && ($_FILES["file"]["size"] < 1000000)
    && in_array($extension, $allowedExts))
  {
    if ($_FILES["file"]["error"] > 0)
    {
      echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    }
    else 
    {
      $fileName = $temp[0].".".$temp[1];
      $temp[0] = rand(0, 3000); //Set to random number
      $fileName;
      $temp = explode(".", $_FILES["file"]["name"]);
      $newfilename = $userRow['id'].round(microtime(true)) . '.' . end($temp);
      move_uploaded_file($_FILES["file"]["tmp_name"], "img/avatar/" . $newfilename);
      $queryperfil = "UPDATE perfil SET avatar = '".$newfilename."' WHERE id = ".$userRow['4'];
      if($MySQLi_CON->query($queryperfil))
      {
       header("Location: editarperfil.php");
     }
     else
     {
      echo "<script>{alert('Registro não foi alterado.');}</script>";
    }
  }
}
else
{
  echo "<script>{alert('Arquivo invalido');}</script>";
}
//   move_uploaded_file($_FILES['file']['tmp_name'], "img/avatar/".$_FILES['file']['name']);
//   $queryperfil = "UPDATE perfil SET avatar = '".$_FILES['file']['name']."' WHERE id = ".$userRow['4'];
//   if($MySQLi_CON->query($queryperfil))
//   {
//    header("Location: editarperfil.php");
//  }
//  else
//  {
//   echo "<script>{alert('Registro não foi alterado.');}</script>";
// }

}

if(isset($_POST['btn-signup2']))
{
 $nome = $MySQLi_CON->real_escape_string(trim($_POST['nome']));
 $ocupacao = $MySQLi_CON->real_escape_string(trim($_POST['ocupacao']));
 $educacao = $MySQLi_CON->real_escape_string(trim($_POST['educacao']));
 $instituicao = $MySQLi_CON->real_escape_string(trim($_POST['instituicao']));
 $uf = $MySQLi_CON->real_escape_string(trim($_POST['uf']));
 $cidade = $MySQLi_CON->real_escape_string(trim($_POST['cidade']));



 $query = "UPDATE perfil SET nome ='$nome', ocupacao ='$ocupacao', educacao ='$educacao', instituicao ='$instituicao', uf='$uf', cidade = '$cidade'
 where id = ".$userRow['4'];


 if($MySQLi_CON->query($query))
 {
   header("Location: editarperfil.php");
 }
 else
 {
  echo "<script>{alert('Registro não foi alterado.');}</script>";
}



$MySQLi_CON->close();
}
?>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Workdone</title>

  <!-- Bootstrap Core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="bootstrap/css/sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">
</head>

<body>
  <div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav bd-dark">
        <li class="sidebar-brand">
          <a href="home.php">
            Workdone
          </a>
        </li>
      </li>
      <!-- SIDEBAR USERPIC -->
      <div class="profile-userpic">
        <img src="<?php
        if($userRow['avatar']== NULL)
        {
          echo "img/avatar/default.jpg";
        } 
        else
        {
          echo "img/avatar/".$userRow['avatar'];
        }
        ?>" 
        class="img-responsive img-circle" style="height:150px; width:150px;"alt="">
      </div>
      <!-- END SIDEBAR USERPIC -->
      <!-- SIDEBAR USER TITLE -->
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">
         <?php echo $userRow['nome']; ?>
       </div>
       <div class="profile-usertitle-job">
        <?php echo $userRow['ocupacao']; ?>
      </div>
    </div>
    <!-- END SIDEBAR USER TITLE -->
    <!-- SIDEBAR MENU -->
    <div class="profile-usermenu">
      <ul class="nav">
        <li class="active">
        </li>
        <li>
          <a href="home.php">
            <i class="glyphicon glyphicon-file"></i>
            Meus Projetos </a>
          </li>
          <li>
            <a href="editarperfil.php">
              <i class="glyphicon glyphicon-user"></i>
              Perfil </a>
            </li>
            <li>
              <a href="contato.php">
                <i class="glyphicon glyphicon-flag"></i>
                Contato </a>
              </li>
              <?php
              if ($userRow['tipo_do_usuario_id'] == 1)
              { 
                echo "<li>";
                echo "<a href=\"admin.php\">";
                echo "<i class=\"glyphicon glyphicon-list-alt\"></i>";
                echo "Gestão de Usuários </a>";
                echo "</li>";
              }
              ?>
              <li>
                <a href="logout.php?logout">
                  <i class="glyphicon glyphicon-log-out"></i>
                  Sair </a>
                </li>

              </ul>
            </div>
            <!-- END MENU -->
          </div>

          <!-- /#sidebar-wrapper -->
          <!-- Page Contenst -->
          <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></a>

          <div class="container">
            <div class="row-centered">
              <h2><br>Editar Perfil</h2>
              <br>
              <br>
              <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                  <h4><br>Alterar Foto de Perfil</h4><br>
                  <div class="well">
                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                      <div class="text-center">
                        <div class="profile-userpic">
                          <img src="<?php
                          if($userRow['avatar']== NULL)
                          {
                            echo "img/avatar/default.jpg";
                          } 
                          else
                          {
                            echo "img/avatar/".$userRow['avatar'];
                          }
                          ?>" 
                          class="img-responsive img-circle" style="height:250px; width:250px;" alt="">
                        </div>
                        <h6>Faça o upload de uma foto nova</h6>

                        <input type="file" name="file" class="form-control"><br>
                        <button type="submit" class="btn btn-primary" name="btn-foto">
                          <span class="glyphicon glyphicon-log-in"></span> &nbsp; Enviar
                        </button><br><br>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <h4><br>Dados do usuário</h4><br>
              <div class="well">
                <form class="form-horizontal" action="" method="post">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Email</label>
                    <div class="col-sm-4 " >
                      <input type="email" value="<?php echo $userRow['email']?>" class="form-control" placeholder="Endereço de Email" name="email" required  />
                      <span id="check-e"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Senha</label>
                    <div class="col-sm-4 " >
                      <input type="text" value="" class="form-control" placeholder="Senha" name="senha" required  />
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary" name="btn-usuario">
                      <span class="glyphicon glyphicon-log-in"></span> &nbsp; Alterar
                    </button><br><br>
                  </div> 
                </form>
              </div>
              <h4><br>Dados do perfil</h4><br>
              <div class="well">
                <form class="form-horizontal" action="" method="post">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Nome Completo</label>
                    <div class="col-sm-4 " >
                      <input type="text" value="<?php echo $userRow['nome']?>" class="form-control" placeholder="Nome Completo" name="nome" required  />
                      <span id="check-e"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Ocupação</label>
                    <div class="col-sm-4 " >
                      <input type="text" value="<?php echo $userRow['ocupacao']?>" class="form-control" placeholder="Ocupação" name="ocupacao" required  />
                      <span id="check-e"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Educação</label>
                    <div class="col-sm-4 " >
                      <input type="text" value="<?php echo $userRow['educacao']?>" class="form-control" placeholder="Educação" name="educacao" required  />
                      <span id="check-e"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Instituição</label>
                    <div class="col-sm-4 " >
                      <input type="text" value="<?php echo $userRow['instituicao']?>" class="form-control" placeholder="Instituição" name="instituicao" required  />
                      <span id="check-e"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Estado</label>
                    <div class="col-sm-4 " >
                      <select class="form-control" name="uf">
                        <option value="">Selecione Seu Estado</option>
                        <option value="AC"<?php if ($userRow['uf'] === 'AC') echo "selected=\"selected\""?>>Acre</option>
                        <option value="AL"<?php if ($userRow['uf'] === 'AL') echo "selected=\"selected\""?>>Alagoas</option>
                        <option value="AP"<?php if ($userRow['uf'] === 'AP') echo "selected=\"selected\""?>>Amapá</option>
                        <option value="AM"<?php if ($userRow['uf'] === 'AM') echo "selected=\"selected\""?>>Amazonas</option>
                        <option value="BA"<?php if ($userRow['uf'] === 'BA') echo "selected=\"selected\""?>>Bahia</option>
                        <option value="CE"<?php if ($userRow['uf'] === 'CE') echo "selected=\"selected\""?>>Ceará</option>
                        <option value="DF"<?php if ($userRow['uf'] === 'DF') echo "selected=\"selected\""?>>Distrito Federal</option>
                        <option value="ES"<?php if ($userRow['uf'] === 'ES') echo "selected=\"selected\""?>>Espirito Santo</option>
                        <option value="GO"<?php if ($userRow['uf'] === 'GO') echo "selected=\"selected\""?>>Goiás</option>
                        <option value="MA"<?php if ($userRow['uf'] === 'MA') echo "selected=\"selected\""?>>Maranhão</option>
                        <option value="MS"<?php if ($userRow['uf'] === 'MS') echo "selected=\"selected\""?>>Mato Grosso do Sul</option>
                        <option value="MT"<?php if ($userRow['uf'] === 'MT') echo "selected=\"selected\""?>>Mato Grosso</option>
                        <option value="MG"<?php if ($userRow['uf'] === 'MG') echo "selected=\"selected\""?>>Minas Gerais</option>
                        <option value="PA"<?php if ($userRow['uf'] === 'PA') echo "selected=\"selected\""?>>Pará</option>
                        <option value="PB"<?php if ($userRow['uf'] === 'PB') echo "selected=\"selected\""?>>Paraíba</option>
                        <option value="PR"<?php if ($userRow['uf'] === 'PR') echo "selected=\"selected\""?>>Paraná</option>
                        <option value="PE"<?php if ($userRow['uf'] === 'PE') echo "selected=\"selected\""?>>Pernambuco</option>
                        <option value="PI"<?php if ($userRow['uf'] === 'PI') echo "selected=\"selected\""?>>Piauí</option>
                        <option value="RJ"<?php if ($userRow['uf'] === 'RJ') echo "selected=\"selected\""?>>Rio de Janeiro</option>
                        <option value="RN"<?php if ($userRow['uf'] === 'RN') echo "selected=\"selected\""?>>Rio Grande do Norte</option>
                        <option value="RS"<?php if ($userRow['uf'] === 'RS') echo "selected=\"selected\""?>>Rio Grande do Sul</option>
                        <option value="RO"<?php if ($userRow['uf'] === 'RO') echo "selected=\"selected\""?>>Rondônia</option>
                        <option value="RR"<?php if ($userRow['uf'] === 'RR') echo "selected=\"selected\""?>>Roraima</option>
                        <option value="SC"<?php if ($userRow['uf'] === 'SC') echo "selected=\"selected\""?>>Santa Catarina</option>
                        <option value="SP"<?php if ($userRow['uf'] === 'SP') echo "selected=\"selected\""?>>São Paulo</option>
                        <option value="SE"<?php if ($userRow['uf'] === 'SE') echo "selected=\"selected\""?>>Sergipe</option>
                        <option value="TO"<?php if ($userRow['uf'] === 'TO') echo "selected=\"selected\""?>>Tocantins</option>
                      </select>
                      <span id="check-e"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Cidade</label>
                    <div class="col-sm-4 " >
                      <input type="text" value="<?php echo $userRow['cidade']?>" class="form-control" placeholder="Cidade" name="cidade" required  />
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary" name="btn-signup2">
                      <span class="glyphicon glyphicon-log-in"></span> &nbsp; Alterar
                    </button><br><br>
                  </div> 
                </form>
              </div>


              <div class="row">
              </div>
            </div>
            <section id="contact">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Contate-nos</h2>
                    <hr class="primary">
                    <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
                  </div>
                  <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>(31) 9 9346-5930</p>
                  </div>
                  <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
                  </div>
                </div>

                <!-- Modal Cadastrar -->
                <div class="modal fade" id="cadastrar" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cadastrar Usuário</h4>
                      </div>
                      <div class="modal-body text-center">
                        <form class="form-horizontal" action="" method="post">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Nome Completo</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Nome Completo" name="nome" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Ocupação</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Ocupação" name="ocupacao" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Educação</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Educação" name="educacao" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Instituição</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Instituição" name="instituicao" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Estado</label>
                            <div class="col-sm-4 " >
                              <select class="form-control" name="uf">
                                <option value="">Selecione Seu Estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                              </select>
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Cidade</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Cidade" name="cidade" required  />
                            </div>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-success" name="btn-signup">
                              <span class="glyphicon glyphicon-log-in"></span> &nbsp; Alterar
                            </button><br><br>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                          </div>

                        </form>

                        <br>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /Modal Cadastrar -->
                <!-- Modal Deletar -->
                <!-- /Modal Deletar -->
                <!-- Modal Mensagem -->
                <div class="modal fade" id="mensagem" role="dialog">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Enviar Mensagem</h4>
                      </div>
                      <div class="modal-body">
                        <textarea class="form-control" rows="3"></textarea>
                        <br>
                        <button type="button" class="btn btn-success pull-right">Enviar<span class="glyphicon glyphicon-send" aria-hidden="true"></span></button>
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <br>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /Modal Mensagem -->
                <!-- Modal Alterar -->
                <div class="modal fade" id="alterar" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alterar Dados do Usuário</h4>
                      </div>
                      <div class="modal-body text-center">
                        <form class="form-horizontal" action="" method="post">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Nome Completo</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Nome Completo" name="nome" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Ocupação</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Ocupação" name="ocupacao" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Educação</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Educação" name="educacao" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Instituição</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Instituição" name="instituicao" required  />
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Estado</label>
                            <div class="col-sm-4 " >
                              <select class="form-control" name="uf">
                                <option value="">Selecione Seu Estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                              </select>
                              <span id="check-e"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Cidade</label>
                            <div class="col-sm-4 " >
                              <input type="text" class="form-control" placeholder="Cidade" name="cidade" required  />
                            </div>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-success" name="btn-signup2">
                              <span class="glyphicon glyphicon-log-in"></span> &nbsp; Alterar
                            </button><br><br>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                          </div>

                        </form>

                        <br>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /Modal Alterar -->
              </div>
            </section>
            <!-- /#page-content-wrapper -->

          </div>
          <!-- /#wrapper -->

          <!-- jQuery -->
          <script src="bootstrap/js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="bootstrap/js/bootstrap.min.js"></script>

          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>

          <?php
          if(isset($_POST['btn-usuario']))
          {
           $email = $MySQLi_CON->real_escape_string(trim($_POST['email']));
           $senha = $MySQLi_CON->real_escape_string(trim($_POST['senha']));




           $query2 = "UPDATE usuario SET email ='$email', senha ='$senha' where id = ".$_SESSION['userSession'];

           if($MySQLi_CON->query($query2))
           {
             header("Location: editarperfil.php");
           }
           else
           {
            echo "<script>{alert('Registro não foi alterado.');}</script>";
          }
        }


        ?>

      </body>

      </html>