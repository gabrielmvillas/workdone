<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['userSession']))
{
 header("Location: signin.php");
}
$id = $_GET['id'];
$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);
$userRow=$query->fetch_array();


if(($userRow['tipo_do_usuario_id']) != 1)
{
 header("Location: home.php");
}

$adminselect = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$id);
$adminRow=$adminselect->fetch_array();

$select = $MySQLi_CON->query("SELECT * FROM documento WHERE usuario_id=".$id);
$linhas=$select->num_rows;

if(isset($_POST['btn-usuario']))
{
 $email = $MySQLi_CON->real_escape_string(trim($_POST['email']));
 $upass = $MySQLi_CON->real_escape_string(trim($_POST['senha']));
 $tipo_do_usuario_id = $_POST['tipo'];
 
 $new_password = password_hash($upass, PASSWORD_DEFAULT);
 
 $check_email = $MySQLi_CON->query("SELECT email FROM usuario WHERE email='$email'");
 $count=$check_email->num_rows;
 
 if($count==0){

  if($upass == '')
  {
    $query = "UPDATE usuario SET email ='$email', tipo_do_usuario_id = $tipo_do_usuario_id where id = ".$adminRow['id'];
  }
  else
  { 
    $query = "UPDATE usuario SET email ='$email', senha ='$new_password', tipo_do_usuario_id = '$tipo_do_usuario_id' where id = ".$adminRow['id'];
  }

  if($MySQLi_CON->query($query))
  {
   header("Location: admin2.php?id=".$adminRow['id']);
 }
 else
 {
  echo "<script>{alert('Registro não foi alterado.');}</script>";
}
}
else
{
  $msg = "<div class='row'>
  <div class='alert alert-danger col-md-4 col-md-offset-4'>
    <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Este e-mail já é cadastrado!
  </div>
</div>";
}
}
if(isset($_POST['btn-perfil']))
{
 $nome = $MySQLi_CON->real_escape_string(trim($_POST['nome']));
 $ocupacao = $MySQLi_CON->real_escape_string(trim($_POST['ocupacao']));
 $educacao = $MySQLi_CON->real_escape_string(trim($_POST['educacao']));
 $instituicao = $MySQLi_CON->real_escape_string(trim($_POST['instituicao']));
 $uf = $MySQLi_CON->real_escape_string(trim($_POST['uf']));
 $cidade = $MySQLi_CON->real_escape_string(trim($_POST['cidade']));

 $query = "UPDATE perfil SET nome ='$nome', ocupacao ='$ocupacao', educacao ='$educacao', instituicao ='$instituicao', uf='$uf', cidade = '$cidade'
 where id = ".$adminRow['4'];

 if($MySQLi_CON->query($query))
 {
   header("Location: editarperfil.php");
 }
 else
 {
  echo "<script>{alert('Registro não foi alterado.');}</script>";
}

}
if(isset($_POST['btn-deletar']))
{ 
  $deletar1 = "DELETE FROM perfil where usuario_id = ".$id;
  $deletar2 = "DELETE FROM usuario where id = ".$id;
  if($MySQLi_CON->query($deletar1))
  {
    echo "woohoo";
  }
  if($MySQLi_CON->query($deletar2))
  {
   header("Location: admin.php");
 }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Workdone</title>

  <!-- Bootstrap Core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="bootstrap/css/sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">
</head>

<body>
  <div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav bd-dark">
        <li class="sidebar-brand">
          <a href="home.php">
            Workdone
          </a>
        </li>
        <a href="admin.php">Gestão de Usuários</a>
      </li>
      <!-- SIDEBAR USERPIC -->
      <div class="profile-userpic">
        <img src="<?php
        if($userRow['avatar']== NULL)
        {
          echo "img/avatar/default.jpg";
        } 
        else
        {
          echo "img/avatar/".$userRow['avatar'];
        }
        ?>" 
        class="img-responsive img-circle" style="height:150px; width:150px;"alt="">
      </div>
      <!-- END SIDEBAR USERPIC -->
      <!-- SIDEBAR USER TITLE -->
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">
         <?php echo $userRow['nome']; ?>
       </div>
       <div class="profile-usertitle-job">
        <?php echo $userRow['ocupacao']; ?>
      </div>
    </div>
    <!-- END SIDEBAR USER TITLE -->
    <!-- SIDEBAR MENU -->
    <div class="profile-usermenu">
      <ul class="nav">
        <li class="active">
        </li>
        <li>
          <a href="home.php">
            <i class="glyphicon glyphicon-file"></i>
            Meus Projetos </a>
          </li>
          <li>
            <a href="editarperfil.php">
              <i class="glyphicon glyphicon-user"></i>
              Perfil </a>
            </li>
            <li>
              <a href="contato.php">
                <i class="glyphicon glyphicon-flag"></i>
                Contato </a>
              </li>
              <li>
                <a href="admin.php">
                  <i class="glyphicon glyphicon-list-alt"></i>
                  Gestão de Usuários </a>
                </li>
                <li>
                  <a href="logout.php?logout">
                    <i class="glyphicon glyphicon-log-out"></i>
                    Sair </a>
                  </li>

                </ul>
              </div>
              <!-- END MENU -->
            </div>

            <!-- /#sidebar-wrapper -->
            <!-- Page Contenst -->
            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></a>
            <section id="gerenciador" class="bg-light">
              <div class="container">
                <div class="row-centered">
                  <h2><br>Gestão de usuários</h2><br>
                  <br>
                  <br>
                  <h4><br>Dados do usuário</h4><br>
                  <div class="well">
                    <form class="form-horizontal" action="" method="post">
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Email</label>
                        <div class="col-sm-4 " >
                          <input type="email" value="<?php echo $adminRow['email']?>" class="form-control" placeholder="Endereço de Email" name="email" required  />
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Senha</label>
                        <div class="col-sm-4 " >
                          <input type="text" class="form-control" placeholder="Senha" name="senha"  />
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Tipo do Usuario</label>
                        <div class="col-sm-4 " >
                          <select class="form-control" name="tipo">
                            <option value="">Selecione o Tipo do Usuario</option>
                            <option value="1"<?php if ($adminRow['tipo_do_usuario_id'] === '1') echo "selected=\"selected\""?>>Administrador</option>
                            <option value="2"<?php if ($adminRow['tipo_do_usuario_id'] === '2') echo "selected=\"selected\""?>>Usuario Comum</option>
                            <option value="3"<?php if ($adminRow['tipo_do_usuario_id'] === '3') echo "selected=\"selected\""?>>Usuario Premium</option>
                          </select>
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="btn-usuario">
                          <span class="glyphicon glyphicon-log-in"></span> &nbsp; Alterar
                        </button><br><br>
                      </div> 
                    </form>
                  </div>
                  <h4><br>Dados do perfil</h4><br>
                  <div class="well">
                    <form class="form-horizontal" action="" method="post">
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Nome Completo</label>
                        <div class="col-sm-4 " >
                          <input type="text" value="<?php echo $adminRow['nome']?>" class="form-control" placeholder="Nome Completo" name="nome" required  />
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Ocupação</label>
                        <div class="col-sm-4 " >
                          <input type="text" value="<?php echo $adminRow['ocupacao']?>" class="form-control" placeholder="Ocupação" name="ocupacao" required  />
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Educação</label>
                        <div class="col-sm-4 " >
                          <input type="text" value="<?php echo $adminRow['educacao']?>" class="form-control" placeholder="Educação" name="educacao" required  />
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Instituição</label>
                        <div class="col-sm-4 " >
                          <input type="text" value="<?php echo $adminRow['instituicao']?>" class="form-control" placeholder="Instituição" name="instituicao" required  />
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Estado</label>
                        <div class="col-sm-4 " >
                          <select class="form-control" name="uf">
                            <option value="">Selecione Seu Estado</option>
                            <option value="AC"<?php if ($adminRow['uf'] === 'AC') echo "selected=\"selected\""?>>Acre</option>
                            <option value="AL"<?php if ($adminRow['uf'] === 'AL') echo "selected=\"selected\""?>>Alagoas</option>
                            <option value="AP"<?php if ($adminRow['uf'] === 'AP') echo "selected=\"selected\""?>>Amapá</option>
                            <option value="AM"<?php if ($adminRow['uf'] === 'AM') echo "selected=\"selected\""?>>Amazonas</option>
                            <option value="BA"<?php if ($adminRow['uf'] === 'BA') echo "selected=\"selected\""?>>Bahia</option>
                            <option value="CE"<?php if ($adminRow['uf'] === 'CE') echo "selected=\"selected\""?>>Ceará</option>
                            <option value="DF"<?php if ($adminRow['uf'] === 'DF') echo "selected=\"selected\""?>>Distrito Federal</option>
                            <option value="ES"<?php if ($adminRow['uf'] === 'ES') echo "selected=\"selected\""?>>Espirito Santo</option>
                            <option value="GO"<?php if ($adminRow['uf'] === 'GO') echo "selected=\"selected\""?>>Goiás</option>
                            <option value="MA"<?php if ($adminRow['uf'] === 'MA') echo "selected=\"selected\""?>>Maranhão</option>
                            <option value="MS"<?php if ($adminRow['uf'] === 'MS') echo "selected=\"selected\""?>>Mato Grosso do Sul</option>
                            <option value="MT"<?php if ($adminRow['uf'] === 'MT') echo "selected=\"selected\""?>>Mato Grosso</option>
                            <option value="MG"<?php if ($adminRow['uf'] === 'MG') echo "selected=\"selected\""?>>Minas Gerais</option>
                            <option value="PA"<?php if ($adminRow['uf'] === 'PA') echo "selected=\"selected\""?>>Pará</option>
                            <option value="PB"<?php if ($adminRow['uf'] === 'PB') echo "selected=\"selected\""?>>Paraíba</option>
                            <option value="PR"<?php if ($adminRow['uf'] === 'PR') echo "selected=\"selected\""?>>Paraná</option>
                            <option value="PE"<?php if ($adminRow['uf'] === 'PE') echo "selected=\"selected\""?>>Pernambuco</option>
                            <option value="PI"<?php if ($adminRow['uf'] === 'PI') echo "selected=\"selected\""?>>Piauí</option>
                            <option value="RJ"<?php if ($adminRow['uf'] === 'RJ') echo "selected=\"selected\""?>>Rio de Janeiro</option>
                            <option value="RN"<?php if ($adminRow['uf'] === 'RN') echo "selected=\"selected\""?>>Rio Grande do Norte</option>
                            <option value="RS"<?php if ($adminRow['uf'] === 'RS') echo "selected=\"selected\""?>>Rio Grande do Sul</option>
                            <option value="RO"<?php if ($adminRow['uf'] === 'RO') echo "selected=\"selected\""?>>Rondônia</option>
                            <option value="RR"<?php if ($adminRow['uf'] === 'RR') echo "selected=\"selected\""?>>Roraima</option>
                            <option value="SC"<?php if ($adminRow['uf'] === 'SC') echo "selected=\"selected\""?>>Santa Catarina</option>
                            <option value="SP"<?php if ($adminRow['uf'] === 'SP') echo "selected=\"selected\""?>>São Paulo</option>
                            <option value="SE"<?php if ($adminRow['uf'] === 'SE') echo "selected=\"selected\""?>>Sergipe</option>
                            <option value="TO"<?php if ($adminRow['uf'] === 'TO') echo "selected=\"selected\""?>>Tocantins</option>
                          </select>
                          <span id="check-e"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Cidade</label>
                        <div class="col-sm-4 " >
                          <input type="text" value="<?php echo $adminRow['cidade']?>" class="form-control" placeholder="Cidade" name="cidade" required  />
                        </div>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="btn-perfil">
                          <span class="glyphicon glyphicon-log-in"></span> &nbsp; Alterar
                        </button><br><br>
                      </div> 
                    </form>
                  </div>
                  <div class="panel panel-primary">
                    <div class="panel-heading">Arquivos do usuário</div>
                    <div class="panel-body">
                      <div class="row">
                        <?php
                        if ($linhas > 0)
                        {
                          for($x = 0; $x < $linhas; $x++){
                            $linha = mysqli_fetch_assoc($select);
                            echo "<div class=\"container\">";
                            echo "<div class=\"col-md-3 well well-lg\">";
                            echo "<img src=\"img/manual_a4.jpg\" alt=\"\" class=\"img-rounded img-responsive\">";
                            echo "<label>".$linha['nome']."</label><br>";
                            echo "<button type=\"button\" class=\"btn btn-primary pull-left\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>Editar</button>";
                            echo "<button type=\"button\" class=\"btn btn-danger pull-right\">Deletar<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></button>";
                            echo "</div>";
                            echo "</div>";
                          }
                        }
                        else
                        {
                          echo "Este usuário não póssui nenhum arquivo de texto.<br><br>";
                        }
                        ?>
                      </div>
                    </div>
                  </div>

                  
                  <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#deletar"><span class="glyphicon glyphicon-trash"></span> &nbsp; Deletar</button>

                </div>
                <!-- Modal Deletar -->
                <div class="modal fade" id="deletar" role="dialog">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deseja Mesmo Deletar Este Usuario?</h4>
                      </div>
                      <div class="modal-body">
                        <p>Esta ação não poderá ser revertida!</p>
                        <form method="post" action="">
                          <div class="form-group">
                            <button type="submit" class="btn btn-danger" name="btn-deletar">
                              <span class="glyphicon glyphicon-trash"></span> &nbsp; Deletar
                            </button><br><br>
                          </div>
                        </form>                          
                        <button type="button" class="btn btn-primary pull-center" data-dismiss="modal">Cancelar<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <br>
                        <br>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /Modal Deletar -->
              </div>
            </section>
            <section id="contact">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Contate-nos</h2>
                    <hr class="primary">
                    <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
                  </div>
                  <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>(31) 9 9346-5930</p>
                  </div>
                  <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
                  </div>
                </div>
              </div>
            </section>
            <!-- /#page-content-wrapper -->

          </div>
          <!-- /#wrapper -->

          <!-- jQuery -->
          <script src="bootstrap/js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="bootstrap/js/bootstrap.min.js"></script>

          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>
        </body>
        </html>