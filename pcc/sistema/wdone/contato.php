<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contato</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="bootstrap/css/creative.css" type="text/css">
    <script src="bootstrap/js/jquery-2.2.3.min.js"></script>
    <script src="bootstrap/js/jquery-2.2.3.js"></script>
    <script src="bootstrap/js/jquery.js"></script>
    <script type="text/javascript" src="http://plixsite.net/~tpa/js/jquery.validate.js"></script>

</head>

<body>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="index.html">WorkDone</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.html">Sobre Nós</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html">Serviços</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="contato.php">Contato</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="register.php">Cadastrar</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="signin.php">Entrar</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <section id="contato" class="bg-dark">
        <aside>
          <div class="container text-center">
            <div class="call-to-action">
            <h2>Contato</h2>
            <hr class="primary"> 
                <form method="post" name="form" action="email.php" id="form">
                    <div class="form-group">
                      <label for="nome">Nome :</label>
                      <input type="text" class="form-control" name="nome" id="nome" placeholder="Digite seu Nome">
                    </div>
                    <br />
                    <div class="form-group">
                      <label for="email">Email :</label>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Digite o E-mail">
                    </div>
                    <br />               
                    <div class="form-group">
                      <label for="produto">Assunto :</label>
                      <input type="text" class="form-control" name="assunto" id="assunto" placeholder="Digite o Assunto">
                    </div>
                    <br />
                    <div class="form-group">
                      <label for="sugestao">Mensagem :</label>
                      <textarea class="form-control" rows="6" id="mensagem" name="mensagem" placeholder="Digite sua Mensagem"></textarea>
                    </div>
                    <br />
                    <button type="submit" name="enviar" class="btn btn-success btn-md btn-success">Enviar</button>
                    <button type="reset" name="limpar" class="btn btn-default btn-md btn-primary">Limpar</button>
                </form>
    </section>

<section id="contato2">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="call-to-action">
                    <h2 class="section-heading">Ou contate-nos por telefone!</h2>
                    <hr class="primary">                     
                </div>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>(31) 9 9346-5930</p>
                </div>
            </div>
        </div>
    </section>    

<script type ="text/javascript">
    function init()
    {
        $("#form").validate({
                rules:
                {
                    nome:{
                        required : true
                    },
                    email:{
                        required: true
                    },
                    assunto:{
                        required : true
                    },
                    mensagem:{
                        required : true
                    }
                },
            messages: {
                nome: {
                    required : "*Digite o Nome"
                    },
                email:{
                    required : "*Digite o E-mail"
                    },
                assunto: {
                    required : "*Digite o Assunto"
                    },
                mensagem: {
                    required : "*Digite a Mensagem"
                    }   
            }
        });
    }   
            $(document).ready(init);
</script>


    <!-- jQuery -->
    <script src="bootstrap/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="bootstrap/js/scrollreveal.min.js"></script>
    <script src="bootstrap/js/jquery.easing.min.js"></script>
    <script src="bootstrap/js/jquery.fittext.js"></script>
    <script src="bootstrap/js/jquery.magnific-popup.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="bootstrap/js/creative.js"></script>


</body>

</html>
