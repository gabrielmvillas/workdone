<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['userSession']))
{
 header("Location: signin.php");
}
//"SELECT nome, email FROM usuario JOIN perfil ON usuario.id=usuario_id;"
$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);/*SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=*/
$userRow=$query->fetch_array();
if(($userRow['tipo_do_usuario_id']) != 1)
{
 header("Location: home.php");
}
$select = $MySQLi_CON->query("SELECT usuario.id, nome, email, tipo_do_usuario_id FROM usuario JOIN perfil ON usuario.id=usuario_id;");
$linhas=$select->num_rows;

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Workdone</title>

  <!-- Bootstrap Core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="bootstrap/css/sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">
</head>

<body>
  <div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav bd-dark">
        <li class="sidebar-brand">
          <a href="home.php">
            Workdone
          </a>
        </li>
      </li>
      <!-- SIDEBAR USERPIC -->
      <div class="profile-userpic">
        <img src="<?php
        if($userRow['avatar']== NULL)
        {
          echo "img/avatar/default.jpg";
        } 
        else
        {
          echo "img/avatar/".$userRow['avatar'];
        }
        ?>" 
        class="img-responsive img-circle" style="height:150px; width:150px;"alt="">
      </div>
      <!-- END SIDEBAR USERPIC -->
      <!-- SIDEBAR USER TITLE -->
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">
         <?php echo $userRow['nome']; ?>
       </div>
       <div class="profile-usertitle-job">
        <?php echo $userRow['ocupacao']; ?>
      </div>
    </div>
    <!-- END SIDEBAR USER TITLE -->
    <!-- SIDEBAR MENU -->
    <div class="profile-usermenu">
      <ul class="nav">
        <li class="active">
        </li>
        <li>
          <a href="home.php">
            <i class="glyphicon glyphicon-file"></i>
            Meus Projetos </a>
          </li>
          <li>
            <a href="editarperfil.php">
              <i class="glyphicon glyphicon-user"></i>
              Perfil </a>
            </li>
            <li>
              <a href="contato.php">
                <i class="glyphicon glyphicon-flag"></i>
                Contato </a>
              </li>
              <li>
                <a href="admin.php">
                  <i class="glyphicon glyphicon-list-alt"></i>
                  Gestão de Usuários </a>
                </li>
                <li>
                  <a href="logout.php?logout">
                    <i class="glyphicon glyphicon-log-out"></i>
                    Sair </a>
                  </li>

                </ul>
              </div>
              <!-- END MENU -->
            </div>

            <!-- /#sidebar-wrapper -->
            <!-- Page Contenst -->
            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></a>
            <section id="gerenciador" class="bg-light">
              <div class="container">
                <div class="row-centered">
                  <h2><br>Gestão de usuários</h2><br>
                  <br>
                  <br>
                  <hr>
                </div>
                <div class="row">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Tipo de Usuario</th>
                          <th>Ações</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        for($x = 0; $x < $linhas; $x++){
                          $linha = mysqli_fetch_assoc($select);
                          if ($linha['tipo_do_usuario_id'] == 1)
                          {
                            $tipo_usuario = "Administrador";
                          }
                          else if ($linha['tipo_do_usuario_id'] == 2)
                          {
                            $tipo_usuario = "Usuario Comum";
                          }
                          else if ($linha['tipo_do_usuario_id'] == 3)
                          {
                            $tipo_usuario = "Usuario Premium";
                          }
                          echo "<tr><td>".$linha['nome']."</td>";
                          echo "<td>".$linha['email']."</td>";
                          echo "<td>".$tipo_usuario."</td>";
                          echo "<td>
                          <a class=\"btn btn-primary\" href=\"admin2.php?id=".$linha['id']."\" role=\"button\"><span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span></a>";
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <a style="width: 300px;"class="btn btn-primary center-block col-sm-04" href="cadastraradmin.php" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Cadastrar novo usuario</a>
              </div>
            </section>
            <section id="contact">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Contate-nos</h2>
                    <hr class="primary">
                    <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
                  </div>
                  <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>(31) 9 9346-5930</p>
                  </div>
                  <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
                  </div>
                </div>
              </div>
            </section>
            <!-- /#page-content-wrapper -->

          </div>
          <!-- /#wrapper -->

          <!-- jQuery -->
          <script src="bootstrap/js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="bootstrap/js/bootstrap.min.js"></script>

          <!-- Menu Toggle Script -->
          <script>
            $("#menu-toggle").click(function(e) {
              e.preventDefault();
              $("#wrapper").toggleClass("toggled");
            });
          </script>

        </body>

        </html>