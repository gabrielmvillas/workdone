﻿selects perfil:

SELECT * FROM alunos
	WHERE sexo = 'F'
	ORDER BY nome ASC;
	
SELECT * FROM alunos
	WHERE sexo = 'M'
	ORDER BY id DESC;
	
selects usuario;

SELECT * FROM usuario	
	WHERE id = 1;
	
SELECT * FROM usuario
	WHERE id < 5;
	
selects documento de texto:

SELECT * FROM documento_de_texto
	WHERE id > 2
		ORDER BY nome ASC;




SELECT * FROM documento_de_texto
	WHERE != 1
		ORDER BY data DESC;
		


selects paragrafo:

SELECT * FROM paragrafo
	WHERE id > 5 AND id != 10;
	

selects folha de rosto:

SELECT nome_orientador
	FROM folha_de_rosto JOIN paragrafo
		ON id =  paragrafo_id;
		
selects pagina desenvolvimento:

SELECT titulo
	FROM pagina_desenvolvimento JOIN paragrafo
		ON id =  paragrafo_id;
		
		
selects capa:

SELECT autor 
	FROM capa	
		WHERE id < 8;
		

selects sumario:

SELECT * FROM sumario
	WHERE id < 4
		ORDER BY nome_sessao ASC;
		
		
selects resumo:

SELECT * FROM resumo;

SELECT titulo 	
	FROM resumo	
		WHERE id != 1;
		


selects pagina conclusao:

SELECT * FROM pagina_conclusao
	WHERE paragrafo_id > 3
		ORDER BY topico DESC;
		

		
selects lista:

SELECT titulo_elementos
	FROM lista
		WHERE numero_paginas != 2;
		


selects pagina introdução

SELECT * FROM pagina_introducao;
	WHERE numero_pagina > 9
		ORDER BY tema ASC;
		
		
selects apendice:

SELECT * FROM apendice
	WHERE numero_pagina < 23
		ORDER BY id DESC;
		
		
selects referencia bibliografica:

SELECT * FROM referencia_bibliografica
	WHERE id < 6
		ORDER BY numero_pagina DESC;




selects dedicatoria_agradecimento:

SELECT * FROM dedicatoria_agradecimento ORDER BY numero_pagina DESC;



