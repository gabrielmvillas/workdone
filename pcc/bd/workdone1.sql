-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28-Out-2016 às 23:02
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `workdone1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `capa`
--
CREATE DATABASE workdone1;
USE workdone1;

CREATE TABLE IF NOT EXISTS `capa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `data` date NOT NULL,
  `localizacao` varchar(250) NOT NULL,
  `instituicao` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `capa`
--

INSERT INTO `capa` (`id`, `titulo`, `data`, `localizacao`, `instituicao`, `documento_id`) VALUES
(9, 'titolevsk', '2016-10-28', 'ASDW', 'introdsus', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conclusao`
--

CREATE TABLE IF NOT EXISTS `conclusao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `desenvolvimento`
--

CREATE TABLE IF NOT EXISTS `desenvolvimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `documento`
--

CREATE TABLE IF NOT EXISTS `documento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `data` date NOT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `documento`
--

INSERT INTO `documento` (`id`, `nome`, `data`, `usuario_id`) VALUES
(1, 'Documento Radical', '2016-10-28', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `integrantes_capa`
--

CREATE TABLE IF NOT EXISTS `integrantes_capa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `numero` smallint(5) unsigned NOT NULL,
  `turma` varchar(20) NOT NULL,
  `capa_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `capa_id` (`capa_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `integrantes_capa`
--

INSERT INTO `integrantes_capa` (`id`, `nome`, `numero`, `turma`, `capa_id`) VALUES
(1, 'i walk without a fear', 1, '1', 8),
(2, 'ESTA', 1, '1', 9),
(3, 'VIDA', 2, '2', 9),
(4, 'E', 3, '3', 9),
(5, 'A', 4, '4', 9),
(6, 'MINHA', 5, '5', 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `introducao`
--

CREATE TABLE IF NOT EXISTS `introducao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `paragrafo_conclusao`
--

CREATE TABLE IF NOT EXISTS `paragrafo_conclusao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conclusao_id` int(10) unsigned NOT NULL,
  `texto` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conclusao_id` (`conclusao_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `paragrafo_desenvolvimento`
--

CREATE TABLE IF NOT EXISTS `paragrafo_desenvolvimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `desenvolvimento_id` int(10) unsigned NOT NULL,
  `texto` varchar(9000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `desenvolvimento_id` (`desenvolvimento_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `paragrafo_introducao`
--

CREATE TABLE IF NOT EXISTS `paragrafo_introducao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `introducao_id` int(10) unsigned NOT NULL,
  `texto` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `introducao_id` (`introducao_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `ocupacao` varchar(45) DEFAULT NULL,
  `educacao` varchar(100) NOT NULL,
  `instituicao` varchar(100) NOT NULL,
  `uf` char(2) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `nome`, `ocupacao`, `educacao`, `instituicao`, `uf`, `cidade`, `usuario_id`, `avatar`) VALUES
(1, 'Cassiana Duarte da Cruz', 'estudante', 'Pedagogia', 'Universidade Federal de Sao Carlos', 'RS', 'Sao Carlos', 1, '11477530517.jpg'),
(2, 'nome completo', 'ocupacao', 'educacao', 'instituicao', 'ES', 'cidade', 2, NULL),
(3, 'Nome Completo', 'ocupacao', 'educacao', 'instituicao', 'MG', 'Belo Horizonte', 10, '31477527509.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `subtopico`
--

CREATE TABLE IF NOT EXISTS `subtopico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topico_id` int(10) unsigned NOT NULL,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `topico_id` (`topico_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sumario`
--

CREATE TABLE IF NOT EXISTS `sumario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_do_usuario`
--

CREATE TABLE IF NOT EXISTS `tipo_do_usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tipo_do_usuario`
--

INSERT INTO `tipo_do_usuario` (`id`, `descricao`) VALUES
(1, 'administrador'),
(2, 'usuario_comum'),
(3, 'usuario_premium');

-- --------------------------------------------------------

--
-- Estrutura da tabela `topico`
--

CREATE TABLE IF NOT EXISTS `topico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sumario_id` int(10) unsigned NOT NULL,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sumario_id` (`sumario_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `tipo_do_usuario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_do_usuario_id` (`tipo_do_usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `email`, `senha`, `tipo_do_usuario_id`) VALUES
(1, 'cassianaa@gmail.com', '123456', 1),
(2, 'woohoo@email.com', '$2y$10$8CKvF/GAf5MwE', 2),
(9, 'easf@as', '$2y$10$7YK3MiwrOeE0G', 1),
(10, 'yea@email.com', '$2y$10$MKHXNFZ3cCpcK', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
