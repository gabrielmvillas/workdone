<?php
session_start();
include_once 'dbconnect.php';
if(!isset($_SESSION['userSession']))
{
  header("Location: signin.php");
}
echo $_SESSION['documentoid'];
$documentoid = $_SESSION['documentoid'];
$topico_nome = $_GET['topico_nome'];
$topico_id= $_GET['topico_id'];
if(isset($_POST['btn-topico']))
{
  print_r($_POST);
  $titulo = $_POST['titulo'];
  $MySQLi_CON->query("UPDATE topico SET nome='$titulo' WHERE id = '$topico_id'");
  header("Location: modalsumario.php?documentoid=$documentoid");
}



?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta charset="utf-8">

<title>Workdone</title>

<!-- Bootstrap Core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>


<!-- Custom CSS -->
  <script type="text/javascript">//<![CDATA[
    // Variable to hold request
    var request;

    $(function () {
      $( "#textao" ).submit(function( event ) {
        $('#pdf').attr('src', 'pdf.php?editor1='+$('#editor1').val());
        event.preventDefault();
    });
  })
</script>
<link href="bootstrap/css/sidebar.css" rel="stylesheet">
<link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
<link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>
    <body>
        <div class="container">
            <div class="row-centered">
            <h2><br>Alterar Tópico</h2><br>
                <br>
            </div>
            <div class="row">
              <form class="form-horizontal" action="modalalterartopico.php?topico_id=<?php echo $topico_id;?>" method="post">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Título</label>
                  <div class="col-sm-10" >
                    <input type="text" class="form-control" value="<?php echo $topico_nome; ?>" name="titulo" required  />
                    <span id="check-e"></span>
                </div>
            </div>
            <div style="display: none;" >
              <input type="text" class="form-control" value="<?php echo $topico_id; ?>" name="topico_id" disabled/>
              <span id="check-e"></span>
          </div><br><br>
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancelar<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
          <div class="form-group">
              <button type="submit" class="btn btn-success pull-right" name="btn-topico">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Alterar
            </button>
        </div>
        <br>
    </form>
</div>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- jQuery -->
<script src="bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Menu Toggle Script -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>



</body>

</html>