<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['userSession']))
{
 header("Location: signin.php");
}
//"SELECT nome, email FROM usuario JOIN perfil ON usuario.id=usuario_id;"
$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);/*SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=*/
$userRow=$query->fetch_array();
if(($userRow['tipo_do_usuario_id']) != 1)
{
 header("Location: home.php");
}
$select = $MySQLi_CON->query("SELECT nome, email, tipo_do_usuario_id FROM usuario JOIN perfil ON usuario.id=usuario_id;");
$linhas=$select->num_rows;

if(isset($_POST['btn-usuario']))
{
 $email = $MySQLi_CON->real_escape_string(trim($_POST['email']));
 $upass = $MySQLi_CON->real_escape_string(trim($_POST['senha']));
 $tipo = $MySQLi_CON->real_escape_string(trim($_POST['tipo']));
 
 $new_password = password_hash($upass, PASSWORD_DEFAULT);
 
 $check_email = $MySQLi_CON->query("SELECT email FROM usuario WHERE email='$email'");
 $count=$check_email->num_rows;
 
 if($count==0){


   $query = "INSERT INTO usuario(id,email,senha,tipo_do_usuario_id) VALUES(null,'$email','$new_password','$tipo')";


   if($MySQLi_CON->query($query))
   {
     $msg = "<div class='row'>
     <div class='alert alert-success col-md-4 col-md-offset-4'>
       <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Registrado com sucesso!
     </div>
   </div>";
   header("Location: cadastraradmin2.php?email=$email");
 }
 else
 {
   $msg = "<div class='row'>
   <div class='alert alert-danger col-md-4 col-md-offset-4'>
     <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Erro no registro!
   </div>
 </div>";
}
}
else{


  $msg = "<div class='row'>
  <div class='alert alert-danger col-md-4 col-md-offset-4'>
    <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Este e-mail já é cadastrado!
  </div>
</div>";

}

$MySQLi_CON->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Workdone</title>

  <!-- Bootstrap Core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="bootstrap/css/sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>

      <body>
        <div id="wrapper">
          <!-- Sidebar -->
          <div id="sidebar-wrapper">
            <ul class="sidebar-nav bd-dark">
              <li class="sidebar-brand">
                <a href="home.php">
                  Workdone
                </a>
              </li>
            </li>
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
              <img src="<?php
              if($userRow['avatar']== NULL)
              {
                echo "img/avatar/default.jpg";
              } 
              else
              {
                echo "img/avatar/".$userRow['avatar'];
              }
              ?>" 
              class="img-responsive img-circle" style="height:150px; width:150px;"alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
              <div class="profile-usertitle-name">
               <?php echo $userRow['nome']; ?>
             </div>
             <div class="profile-usertitle-job">
              <?php echo $userRow['ocupacao']; ?>
            </div>
          </div>
          <!-- END SIDEBAR USER TITLE -->
          <!-- SIDEBAR MENU -->
          <div class="profile-usermenu">
            <ul class="nav">
              <li class="active">
              </li>
              <li>
                <a href="home.php">
                  <i class="glyphicon glyphicon-file"></i>
                  Meus Projetos </a>
                </li>
                <li>
                  <a href="editarperfil.php">
                    <i class="glyphicon glyphicon-user"></i>
                    Perfil </a>
                  </li>
                  <li>
                    <a href="contato.php">
                      <i class="glyphicon glyphicon-flag"></i>
                      Contato </a>
                    </li>
                    <li>
                      <a href="admin.php">
                        <i class="glyphicon glyphicon-list-alt"></i>
                        Gestão de Usuários </a>
                      </li>
                      <li>
                        <a href="logout.php?logout">
                          <i class="glyphicon glyphicon-log-out"></i>
                          Sair </a>
                        </li>

                      </ul>
                    </div>
                    <!-- END MENU -->
                  </div>

                  <!-- /#sidebar-wrapper -->


                  <!-- Page Contenst -->

                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></a>
                  <section id="gerenciador" class="bg-light">
                    <div class="container">
                      <div class="row-centered">
                        <h2><br>Cadastrar usuário</h2><br>
                        <br>
                        <br>
                        <div class="well">
                          <form class="form-horizontal" action="" method="post">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Email</label>
                              <div class="col-sm-4 " >
                                <input type="email" value="" class="form-control" placeholder="Endereço de Email" name="email" required  />
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Senha</label>
                              <div class="col-sm-4 " >
                                <input type="text" class="form-control" placeholder="Senha" name="senha"  />
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Tipo do Usuario</label>
                              <div class="col-sm-4 " >
                                <select class="form-control" name="tipo">
                                  <option value="">Selecione o Tipo do Usuario</option>
                                  <option value="1">Administrador</option>
                                  <option value="2">Usuario Comum</option>
                                  <option value="3">Usuario Premium</option>
                                </select>
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <button type="submit" class="btn btn-primary" name="btn-usuario">
                                <span class="glyphicon glyphicon-log-in"></span> &nbsp; Cadastrar
                              </button><br><br>
                            </div> 
                          </form>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="contact">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 text-center">
                          <h2 class="section-heading">Contate-nos</h2>
                          <hr class="primary">
                          <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
                        </div>
                        <div class="col-lg-4 col-lg-offset-2 text-center">
                          <i class="fa fa-phone fa-3x sr-contact"></i>
                          <p>(31) 9 9346-5930</p>
                        </div>
                        <div class="col-lg-4 text-center">
                          <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                          <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
                        </div>
                      </div>
                    </div>
                  </section>
                  <!-- /#page-content-wrapper -->

                </div>
                <!-- /#wrapper -->

                <!-- jQuery -->
                <script src="bootstrap/js/jquery.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="bootstrap/js/bootstrap.min.js"></script>

                <!-- Menu Toggle Script -->
                <script>
                  $("#menu-toggle").click(function(e) {
                    e.preventDefault();
                    $("#wrapper").toggleClass("toggled");
                  });
                </script>
              </body>
              </html>