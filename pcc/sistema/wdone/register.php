<?php
session_start();
if(isset($_SESSION['userSession'])!="")
{
 header("Location: home.php");
}
include_once 'dbconnect.php';

if(isset($_POST['btn-signup']))
{
 $email = $MySQLi_CON->real_escape_string(trim($_POST['email']));
 $upass = $MySQLi_CON->real_escape_string(trim($_POST['senha']));
 
 $new_password = password_hash($upass, PASSWORD_DEFAULT);
 
 $check_email = $MySQLi_CON->query("SELECT email FROM usuario WHERE email='$email'");
 $count=$check_email->num_rows;
 
 if($count==0){


  $query = "INSERT INTO usuario(id,email,senha,tipo_do_usuario_id) VALUES(null,'$email','$new_password',2)";

  
  if($MySQLi_CON->query($query))
  {
   $msg = "<div class='row'>
   <div class='alert alert-success col-md-4 col-md-offset-4'>
     <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Registrado com sucesso!
   </div>
 </div>";
 header("Location: register2.php?email=$email");
}
else
{
 $msg = "<div class='row'>
 <div class='alert alert-danger col-md-4 col-md-offset-4'>
   <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Erro no registro!
 </div>
</div>";
}
}
else{


  $msg = "<div class='row'>
  <div class='alert alert-danger col-md-4 col-md-offset-4'>
    <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Este e-mail já é cadastrado!
  </div>
</div>";

}

$MySQLi_CON->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>WorkDone</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <!--   <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">  -->
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">

</head>
<body id="page-top">
  <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="index.html">WorkDone</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.html">Sobre Nós</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html">Serviços</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="contato.php">Contato</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="register.php">Cadastrar</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="signin.php">Entrar</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

  <section id="sim" class="bg-dark">
    <aside>
      <div class="container text-center">
        <div class="call-to-action">

          <h2><br>Comece já!</h2>

          <?php
          if(isset($msg)){
           echo $msg;
         }
         else{
           ?>
           <div class="row">
             <div class='alert alert-info col-md-4 col-md-offset-4'>
              <span class='glyphicon glyphicon-asterisk'></span> &nbsp; Todos os campos são obrigatorios
            </div>
          </div>
          <?php
        }
        ?>
        <form class="form-horizontal" action="" method="post">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Email</label>
            <div class="col-sm-4 " >
              <input type="email" class="form-control" placeholder="Endereço de Email" name="email" required  />
              <span id="check-e"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Senha</label>
            <div class="col-sm-4 " >
              <input type="password" class="form-control" placeholder="Senha" name="senha" required  />
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="btn-signup">
              <span class="glyphicon glyphicon-log-in"></span> &nbsp; Cadastrar
            </button><br><br>
            <a href="signin.php" style="float:center; color:#fff;">Ja tenho uma conta</a> 
          </div> 
        </form>
      </div>
    </div>
  </aside>
</section>
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 text-center">
      <h2 class="section-heading">Contate-nos</h2>
        <hr class="primary">
          <p>Tem sugestões, dúvidas ou reclamações? Basta clicar no botão abaixo. Estamos ansiosos para receber seu feedback!</p>
          <a href="contato.php" class="btn btn-success btn-lg sr-button">Contate-nos</a>
      </div>
      <div class="col-lg-4 col-lg-offset-2 text-center">
        <i class="fa fa-phone fa-3x sr-contact"></i>
        <p>(31) 9 9346-5930</p>
      </div>
      <div class="col-lg-4 text-center">
        <i class="fa fa-envelope-o fa-3x sr-contact"></i>
        <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
      </div>
    </div>
  </div>
</section>



<!-- 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="js/scrollreveal.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.fittext.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/creative.js"></script>
</body>
</html>