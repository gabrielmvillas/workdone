
<?php
session_start();
include_once 'dbconnect.php';

if(!isset($_SESSION['userSession']))
{
 header("Location: signin.php");
}
//"SELECT nome, email FROM usuario JOIN perfil ON usuario.id=usuario_id;"
$query = $MySQLi_CON->query("SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=".$_SESSION['userSession']);/*SELECT * FROM usuario JOIN perfil ON usuario.id = perfil.usuario_id WHERE usuario.id=*/
$userRow=$query->fetch_array();
if(($userRow['tipo_do_usuario_id']) != 1)
{
 header("Location: home.php");
}
if(isset($_POST['btn-perfil']))
{
 $nome = $MySQLi_CON->real_escape_string(trim($_POST['nome']));
 $ocupacao = $MySQLi_CON->real_escape_string(trim($_POST['ocupacao']));
 $educacao = $MySQLi_CON->real_escape_string(trim($_POST['educacao']));
 $instituicao = $MySQLi_CON->real_escape_string(trim($_POST['instituicao']));
 $uf = $MySQLi_CON->real_escape_string(trim($_POST['uf']));
 $cidade = $MySQLi_CON->real_escape_string(trim($_POST['cidade']));
 $email = $_GET['email'];

 $id_query = $MySQLi_CON->query("SELECT * FROM usuario WHERE email='$email'");
 $row=$id_query->fetch_array();
 $id_usuario = $row['id'];

 $query = "INSERT INTO perfil(id,nome,ocupacao,educacao,instituicao,uf,cidade,usuario_id) VALUES(null,'$nome','$ocupacao','$educacao','$instituicao','$uf','$cidade','$id_usuario')";


 if($MySQLi_CON->query($query))
 {
   $msg = "<div class='row'>
   <div class='alert alert-success col-md-4 col-md-offset-4'>
     <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Perfil registrado com sucesso!
   </div>
 </div>";
 header("Location: admin.php");
}
else
{
 $msg = "<div class='row'>
 <div class='alert alert-danger col-md-4 col-md-offset-4'>
   <span class='glyphicon glyphicon-info-sign'></span> &nbsp; Erro no registro do perfil!
 </div>
</div>";
}

$MySQLi_CON->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Workdone</title>

  <!-- Bootstrap Core CSS -->
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="bootstrap/css/sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/creative2.css" type="text/css">
  <link rel="stylesheet" href="bootstrap/css/custom.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Days+One" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>

      <body>
        <div id="wrapper">
          <!-- Sidebar -->
          <div id="sidebar-wrapper">
            <ul class="sidebar-nav bd-dark">
              <li class="sidebar-brand">
                <a href="home.php">
                  Workdone
                </a>
              </li>
            </li>
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
              <img src="<?php
              if($userRow['avatar']== NULL)
              {
                echo "img/avatar/default.jpg";
              } 
              else
              {
                echo "img/avatar/".$userRow['avatar'];
              }
              ?>" 
              class="img-responsive img-circle" style="height:150px; width:150px;"alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
              <div class="profile-usertitle-name">
               <?php echo $userRow['nome']; ?>
             </div>
             <div class="profile-usertitle-job">
              <?php echo $userRow['ocupacao']; ?>
            </div>
          </div>
          <!-- END SIDEBAR USER TITLE -->
          <!-- SIDEBAR MENU -->
          <div class="profile-usermenu">
            <ul class="nav">
              <li class="active">
              </li>
              <li>
                <a href="home.php">
                  <i class="glyphicon glyphicon-file"></i>
                  Meus Projetos </a>
                </li>
                <li>
                  <a href="editarperfil.php">
                    <i class="glyphicon glyphicon-user"></i>
                    Perfil </a>
                  </li>
                  <li>
                    <a href="contato.php">
                      <i class="glyphicon glyphicon-flag"></i>
                      Contato </a>
                    </li>
                    <li>
                      <a href="admin.php">
                        <i class="glyphicon glyphicon-list-alt"></i>
                        Gestão de Usuários </a>
                      </li>
                      <li>
                        <a href="logout.php?logout">
                          <i class="glyphicon glyphicon-log-out"></i>
                          Sair </a>
                        </li>

                      </ul>
                    </div>
                    <!-- END MENU -->
                  </div>

                  <!-- /#sidebar-wrapper -->


                  <!-- Page Contenst -->

                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></a>
                  <section id="gerenciador" class="bg-light">
                    <div class="container">
                      <div class="row-centered">
                        <h4><br>Dados do perfil</h4><br>
                        <div class="well">
                          <form class="form-horizontal" action="" method="post">
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Nome Completo</label>
                              <div class="col-sm-4 " >
                                <input type="text" value="" class="form-control" placeholder="Nome Completo" name="nome" required  />
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Ocupação</label>
                              <div class="col-sm-4 " >
                                <input type="text" value="" class="form-control" placeholder="Ocupação" name="ocupacao" required  />
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Educação</label>
                              <div class="col-sm-4 " >
                                <input type="text" value="" class="form-control" placeholder="Educação" name="educacao" required  />
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Instituição</label>
                              <div class="col-sm-4 " >
                                <input type="text" value="" class="form-control" placeholder="Instituição" name="instituicao" required  />
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Estado</label>
                              <div class="col-sm-4 " >
                                <select class="form-control" name="uf">
                                  <option value="">Selecione Seu Estado</option>
                                  <option value="AC">Acre</option>
                                  <option value="AL">Alagoas</option>
                                  <option value="AP">Amapá</option>
                                  <option value="AM">Amazonas</option>
                                  <option value="BA">Bahia</option>
                                  <option value="CE">Ceará</option>
                                  <option value="DF">Distrito Federal</option>
                                  <option value="ES">Espirito Santo</option>
                                  <option value="GO">Goiás</option>
                                  <option value="MA">Maranhão</option>
                                  <option value="MS">Mato Grosso do Sul</option>
                                  <option value="MT">Mato Grosso</option>
                                  <option value="MG">Minas Gerais</option>
                                  <option value="PA">Pará</option>
                                  <option value="PB">Paraíba</option>
                                  <option value="PR">Paraná</option>
                                  <option value="PE">Pernambuco</option>
                                  <option value="PI">Piauí</option>
                                  <option value="RJ">Rio de Janeiro</option>
                                  <option value="RN">Rio Grande do Norte</option>
                                  <option value="RS">Rio Grande do Sul</option>
                                  <option value="RO">Rondônia</option>
                                  <option value="RR">Roraima</option>
                                  <option value="SC">Santa Catarina</option>
                                  <option value="SP">São Paulo</option>
                                  <option value="SE">Sergipe</option>
                                  <option value="TO">Tocantins</option>
                                </select>
                                <span id="check-e"></span>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label col-sm-offset-2">Cidade</label>
                              <div class="col-sm-4 " >
                                <input type="text" value="" class="form-control" placeholder="Cidade" name="cidade" required  />
                              </div>
                            </div>
                            <div class="form-group">
                              <button type="submit" class="btn btn-primary" name="btn-perfil">
                                <span class="glyphicon glyphicon-log-in"></span> &nbsp; Cadastrar
                              </button><br><br>
                            </div> 
                          </form>
                        </div>

                      </div>

                      <div class="row">
                      </div>
                    </div>
                  </section>
                  <section id="contact">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 text-center">
                          <h2 class="section-heading">Contate-nos</h2>
                          <hr class="primary">
                          <p>Tem sugestões, dúvidas ou reclamações? Basta nos contactar pelo telefone ou pelo e-mail abaixo. Estamos ansiosos para obter seu feedback!</p>
                        </div>
                        <div class="col-lg-4 col-lg-offset-2 text-center">
                          <i class="fa fa-phone fa-3x sr-contact"></i>
                          <p>(31) 9 9346-5930</p>
                        </div>
                        <div class="col-lg-4 text-center">
                          <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                          <p><a href="mailto:your-email@your-domain.com">feedback@workdone.com</a></p>
                        </div>
                      </div>
                    </div>
                  </section>
                  <!-- /#page-content-wrapper -->

                </div>
                <!-- /#wrapper -->

                <!-- jQuery -->
                <script src="bootstrap/js/jquery.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="bootstrap/js/bootstrap.min.js"></script>

                <!-- Menu Toggle Script -->
                <script>
                  $("#menu-toggle").click(function(e) {
                    e.preventDefault();
                    $("#wrapper").toggleClass("toggled");
                  });
                </script>
              </body>
              </html>