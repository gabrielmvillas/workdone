-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 31-Out-2016 às 04:08
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `workdone1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `capa`

CREATE DATABASE workdone1;
	USE workdone1;
--

CREATE TABLE IF NOT EXISTS `capa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `data` date NOT NULL,
  `localizacao` varchar(250) NOT NULL,
  `instituicao` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `capa`
--

INSERT INTO `capa` (`id`, `titulo`, `data`, `localizacao`, `instituicao`, `documento_id`) VALUES
(9, 'porraa', '0000-00-00', 'localization', 'fraga', 1),
(10, 'cleitanszeits', '2016-10-31', 'sua mae', 'fodas', 0),
(11, 'cleitanszeits', '2016-10-31', 'sua mae', 'fodas', 2),
(12, 'vtncccccc', '2016-10-31', 'jutds', 'caraioooo', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conclusao`
--

CREATE TABLE IF NOT EXISTS `conclusao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `conclusao`
--

INSERT INTO `conclusao` (`id`, `nome`, `documento_id`) VALUES
(2, 'uai', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `desenvolvimento`
--

CREATE TABLE IF NOT EXISTS `desenvolvimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `desenvolvimento`
--

INSERT INTO `desenvolvimento` (`id`, `nome`, `documento_id`) VALUES
(1, 'ijtijdsi', 1),
(2, '', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `documento`
--

CREATE TABLE IF NOT EXISTS `documento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `data` date NOT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `documento`
--

INSERT INTO `documento` (`id`, `nome`, `data`, `usuario_id`) VALUES
(1, 'Documento Radical', '2016-10-28', 1),
(3, 'merdinha', '2016-10-31', 1),
(7, 'fasdw', '2016-10-31', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `integrantes_capa`
--

CREATE TABLE IF NOT EXISTS `integrantes_capa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `numero` smallint(5) unsigned NOT NULL,
  `turma` varchar(20) NOT NULL,
  `capa_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `capa_id` (`capa_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `integrantes_capa`
--

INSERT INTO `integrantes_capa` (`id`, `nome`, `numero`, `turma`, `capa_id`) VALUES
(1, 'i walk without a fear', 1, '1', 8),
(2, 'ESTA', 1, '1', 9),
(3, 'VIDA', 2, '2', 9),
(4, 'E', 3, '3', 9),
(5, 'A', 4, '4', 9),
(6, 'MINHAa', 5, '5', 9),
(7, 'cleitans', 1, '51', 10),
(8, 'maridosens', 4, '51', 10),
(9, 'spiricleids', 3, '44', 10),
(10, 'flesens', 8, '45', 10),
(11, 'frozens', 5, '19', 10),
(12, 'adw', 1, '4', 11),
(13, 'adwwdw', 4, '6', 11),
(14, 'dwdwdwdw', 8, '4', 11),
(15, 'asasasasas', 5, '9', 11),
(16, 'dezoito', 19, '20', 11),
(17, 'as', 1, '1', 12),
(18, 'awda', 2, '2', 12),
(19, 'fefsa', 6, '3', 12),
(20, 'jfddfs', 4, '4', 12),
(21, 'luoytrwe', 5, '5', 12);

-- --------------------------------------------------------

--
-- Estrutura da tabela `introducao`
--

CREATE TABLE IF NOT EXISTS `introducao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `introducao`
--

INSERT INTO `introducao` (`id`, `nome`, `documento_id`) VALUES
(5, 'introasd', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paragrafo_conclusao`
--

CREATE TABLE IF NOT EXISTS `paragrafo_conclusao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conclusao_id` int(10) unsigned NOT NULL,
  `texto` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conclusao_id` (`conclusao_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `paragrafo_conclusao`
--

INSERT INTO `paragrafo_conclusao` (`id`, `conclusao_id`, `texto`) VALUES
(2, 2, 'ze');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paragrafo_desenvolvimento`
--

CREATE TABLE IF NOT EXISTS `paragrafo_desenvolvimento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `desenvolvimento_id` int(10) unsigned NOT NULL,
  `texto` varchar(9000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `desenvolvimento_id` (`desenvolvimento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `paragrafo_desenvolvimento`
--

INSERT INTO `paragrafo_desenvolvimento` (`id`, `desenvolvimento_id`, `texto`) VALUES
(1, 1, 'spirucleids'),
(2, 1, 'flesens'),
(3, 1, 'flesens'),
(4, 1, 'mano'),
(5, 1, 'ze');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paragrafo_introducao`
--

CREATE TABLE IF NOT EXISTS `paragrafo_introducao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `introducao_id` int(10) unsigned NOT NULL,
  `texto` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `introducao_id` (`introducao_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `paragrafo_introducao`
--

INSERT INTO `paragrafo_introducao` (`id`, `introducao_id`, `texto`) VALUES
(3, 5, 'askdjwidwdw'),
(4, 5, 'vaitomanocuuuuuuu');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `ocupacao` varchar(45) DEFAULT NULL,
  `educacao` varchar(100) NOT NULL,
  `instituicao` varchar(100) NOT NULL,
  `uf` char(2) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `nome`, `ocupacao`, `educacao`, `instituicao`, `uf`, `cidade`, `usuario_id`, `avatar`) VALUES
(1, 'Cassiana Duarte da Cruz', 'estudante', 'Pedagogia', 'Universidade Federal de Sao Carlos', 'RS', 'Sao Carlos', 1, '11477530517.jpg'),
(2, 'nome completo', 'ocupacao', 'educacao', 'instituicao', 'ES', 'cidade', 2, NULL),
(3, 'Nome Completo', 'ocupacao', 'educacao', 'instituicao', 'MG', 'Belo Horizonte', 10, '31477527509.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `subtopico`
--

CREATE TABLE IF NOT EXISTS `subtopico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topico_id` int(10) unsigned NOT NULL,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `topico_id` (`topico_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `subtopico`
--

INSERT INTO `subtopico` (`id`, `topico_id`, `nome`) VALUES
(1, 1, 'subtopicoaaauuu12'),
(2, 1, 'eitaopowsh'),
(3, 3, 'bacana'),
(4, 3, 'banaca'),
(5, 4, 'asdw'),
(6, 1, '1 role'),
(7, 1, '2 role'),
(8, 1, '3 role'),
(9, 1, '4role'),
(10, 1, '5 role'),
(11, 1, '6 role'),
(12, 1, 'yahaaauuuu1'),
(13, 1, 'yahaaauuuu1'),
(14, 13, 'spiricleeesens'),
(15, 5, 'fodas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sumario`
--

CREATE TABLE IF NOT EXISTS `sumario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documento_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documento_id` (`documento_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `sumario`
--

INSERT INTO `sumario` (`id`, `documento_id`) VALUES
(1, 1),
(2, 0),
(3, 0),
(4, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_do_usuario`
--

CREATE TABLE IF NOT EXISTS `tipo_do_usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tipo_do_usuario`
--

INSERT INTO `tipo_do_usuario` (`id`, `descricao`) VALUES
(1, 'administrador'),
(2, 'usuario_comum'),
(3, 'usuario_premium');

-- --------------------------------------------------------

--
-- Estrutura da tabela `topico`
--

CREATE TABLE IF NOT EXISTS `topico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sumario_id` int(10) unsigned NOT NULL,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sumario_id` (`sumario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `topico`
--

INSERT INTO `topico` (`id`, `sumario_id`, `nome`) VALUES
(1, 1, 'yahaaauuuu4'),
(3, 1, 'vreeeiiii'),
(4, 1, 'thiiiis liiiife'),
(5, 0, 'bela foda'),
(6, 0, 'asdfasdfg'),
(7, 0, 'flesens'),
(8, 0, 'dis laaif'),
(9, 0, 'hiaudwhiua'),
(10, 0, 'Mâ™¥â™¥'),
(11, 0, 'ffssddwdwdw'),
(12, 0, 'vsf viado'),
(13, 1, 'spiriclesens'),
(14, 1, 'asdw'),
(15, 1, 'eitaporra'),
(16, 1, 'jhsl'),
(17, 0, 'delicia'),
(18, 0, 'vsf carai'),
(19, 0, 'vsf'),
(20, 0, 'cu');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `tipo_do_usuario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_do_usuario_id` (`tipo_do_usuario_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `email`, `senha`, `tipo_do_usuario_id`) VALUES
(1, 'cassianaa@gmail.com', '123456', 1),
(2, 'woohoo@email.com', '$2y$10$8CKvF/GAf5MwE', 2),
(9, 'easf@as', '$2y$10$7YK3MiwrOeE0G', 1),
(10, 'yea@email.com', '$2y$10$MKHXNFZ3cCpcK', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
